package com.guild.mykeyreg.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.tavant.base.DriverFactory;
import com.tavant.kwutils.KWVariables;
import com.tavant.utils.TwfException;
import jxl.read.biff.BiffException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class Util {

	/**
	 * Add Failure Reason to Report
	 * 
	 * @param description
	 * @param actualValue
	 * @param expectedValue
	 * @return
	 * @throws TwfException
	 */
	public static void addExceptionToReport(String description, String actualValue, String expectedValue)
			throws TwfException {
		throw new TwfException(description + " :<font color=\"solid orange\">  Actual :[" + actualValue
				+ "]</font><font color=\"EE7600\"> Expected :[" + expectedValue + "]</font><br> <b>Step Details:</b> "
				+ "<br>");
	}

	/**
	 * Gets the window handles and switches between the windows
	 * 
	 * @throws TwfException
	 * 
	 * @throws InterruptedException
	 */
	public static void switchToWindow(WebDriver driver) throws TwfException, InterruptedException {
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			System.out.println("winHandle ==> " + winHandle);
		}
	}

	public static boolean isElementPresentOnPage(WebDriver driver, By value) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		if (driver.findElements(value).size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean ischeckBoxSelected(WebDriver driver, By value) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		if (driver.findElements(value).size() > 0) {
			if (driver.findElement(value).isSelected()) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	/**
	 * Verifies the option box status, its its not selected then it selects,
	 * 
	 * @param expectedCheckboxStatus
	 * @param weCheckbox
	 * @throws InterruptedException
	 */
	public static void selectCheckBoxOption(WebElement weCheckbox) throws InterruptedException {
		if (!weCheckbox.isSelected()) {
			weCheckbox.click();
			Thread.sleep(1000);
		} else {
			System.out.println("Option already selected");
		}
	}

	/**
	 * Click Alert OK button and verifies the alert text
	 * 
	 * @author muni.reddy
	 * @param value
	 * @throws TwfException
	 * @throws BiffExceptiona
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public static void getAlertpopup(String val, String borrName)
			throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
		WebDriver driver = DriverFactory.getDriver();
		String actualAlertText;
		String paramsFromActionSteps[] = val.split(",");
		String[] arr = KWVariables.getVariables().get(paramsFromActionSteps[0]).split(":");
		String expectedAlertText = arr[1].replace(arr[2], borrName).trim();
		try {
			Thread.sleep(1000);
			driver.findElement(By.id(arr[0])).click();
			Thread.sleep(1000);
			Alert alert = driver.switchTo().alert();
			actualAlertText = alert.getText().trim();
			alert.accept();
			if (!actualAlertText.equalsIgnoreCase(expectedAlertText)) {
				Util.addExceptionToReport("Alert text mismatch", actualAlertText, expectedAlertText);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Get date based on the format and the numbers of days requested by adding
	 * / subtracting
	 * 
	 * @author muni.reddy
	 * @param strDateFormat
	 * @param days
	 * @return
	 */
	public static String getDateToSearch(String strDateFormat, String expectedType, int days) {
		SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		GregorianCalendar cal = new GregorianCalendar();
		Date date = new Date();
		cal.setTime(date);
		if (expectedType.equalsIgnoreCase("days")) {
			cal.add(Calendar.DATE, days);
		} else if (expectedType.equalsIgnoreCase("month")) {
			cal.add(Calendar.MONTH, days);
		} else if (expectedType.equalsIgnoreCase("year")) {
			cal.add(Calendar.YEAR, days);
		}
		return sdf.format(cal.getTime());
	}

	/**
	 * Selects the radio button based input value give,
	 * 
	 * @param expectedCheckboxStatus
	 * @param weCheckbox
	 * @throws InterruptedException
	 */
	public static void selectRadiobutton(WebElement weCheckbox) throws InterruptedException {
		if (!weCheckbox.isSelected()) {
			weCheckbox.click();
			Thread.sleep(1000);
		} else {
			System.out.println("Option already selected");
		}
	}

	public static void deleteAllCookies(WebDriver driver) {
		System.out.println("*************** deleteAllCookies ************");
		driver.manage().deleteAllCookies();
	}
public static void selectDropdownOption(WebElement selectOption, String optionToSelect, String value){
		System.out.println("*********** selectDropdownOption ************");
		
		Select options = new Select(selectOption);
		switch (optionToSelect){
		case "displayedtext":
			options.selectByVisibleText(optionToSelect);
			break;
		case "value":
			options.selectByValue(optionToSelect);
			break;
		case "index":
			options.selectByIndex(Integer.parseInt(optionToSelect.trim()));
			break;
		default:
			options.selectByValue(optionToSelect);
			break;
		}
	}
}