package com.guild.mykeyreg.pages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.guild.mykeyreg.util.*;
import com.tavant.base.DriverFactory;
import com.tavant.base.WebPage;
import com.tavant.kwutils.KWVariables;
import com.tavant.kwutils.MyTestCaseExecuter;
import com.tavant.utils.TwfException;
import jxl.read.biff.BiffException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class MyKey extends WebPage {
	WebDriver driver;
	int elementDisplayTimeOut = 30;
	static String borrowerName;
	static boolean guildToGuildRft = false;
	public static String loanNumber;
	public static String branchCode;
	public static String applicationId;
	static String respaDate;
	static ArrayList<String> applicationValues;
	static ArrayList<String> columnNames;
	public static ArrayList<String> selectedDropdownOptions = new ArrayList<String>();
	static String selectedOption, selectedDDOption, selectedOptionValue;
	static String purposeOfLoan;
	static String parenthandle;
	static String selectedLoanProgram;
	static String loanProgramId;
	static String appraisalValue;
	static String saveAndContinueBtn, loanProgramd_dd, loanProgramDD, submitToUnderwritingType;;
	static String submitToUnderwriting, credit_val, suCreditRtbn, suPPropertyRbtn, suFullRbtn;
	static String property_val, full_val, purchase_val;
	static String ssn1_txb, ssn2_txb, ssn3_txb, homePhoneNumber1_txb, homePhoneNumber2_txb, homePhoneNumber3_txb;
	static String purposeOfLoan_dd, reoSubjectProperty_No_rbtn, reoSubjectProperty_Yes_rbtn;
	static String CBssn1_txb, CBssn2_txb, CBssn3_txb, cb_HomePhoneNumber1_txb, cb_HomePhoneNumber2_txb,
			cb_HomePhoneNumber3_txb;
	String visible = "CheckVisible";
	String notVisible = "CheckNotVisible";
	String selected = "Checkselected";
	String notselected = "CheckNotSelected";

	/**
	 * Verifies the login functionality with valid user credentials
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */

	public void verifyLogin(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************* verifyLogin(String values) ************* ");
		String params = KWVariables.getVariables().get(values);
		String args[] = params.split(":");
		getElementByUsing(args[0]).clear();
		getElementByUsing(args[0]).sendKeys(args[5]);
		Thread.sleep(1000);
		getElementByUsing(args[1]).clear();
		getElementByUsing(args[1]).sendKeys(args[6]);
		getElementByUsing(args[2]).click();
		// Wait for Clear Filters button
		waitForElement(getElementByUsing(args[3]), elementDisplayTimeOut);
		String homePageMyAccLink = getElementByUsing(args[4]).getText();
		if (!homePageMyAccLink.equalsIgnoreCase(args[8])) {
			Util.addExceptionToReport("Login Failed", homePageMyAccLink, args[8]);
		}
	}

	/**
	 * Gets the window handles and switches to the window
	 * 
	 * @author muni.reddy
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void switchToWindow(String values) throws TwfException, InterruptedException {
		System.out.println("*********** switchToWindow *********** ");
		WebDriver driver = DriverFactory.getDriver();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			System.out.println("Switch to window is done");
		}
	}

	/**
	 * Gets the branch and application ids
	 * 
	 * @param value
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public void getBranchAndApplicationNumber(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("******************** getBranchAndApplicationNumber **********************");
		applicationValues = new ArrayList<String>();
		driver = DriverFactory.getDriver();
		String args = KWVariables.getVariables().get(value);
		String borrowerInfo = getElementByUsing(args).getText().replace("   ", " ").trim();
		if (borrowerInfo.length() > 1) {
			String loanInfo[] = borrowerInfo.split(" ");
			branchCode = loanInfo[1].split("-")[0];
			applicationId = loanInfo[1].split("-")[1];
			borrowerName = loanInfo[2].replace(",", "") + " " + loanInfo[3];
			applicationValues.add(branchCode);
			applicationValues.add(applicationId);
			applicationValues.add(borrowerName);
			System.out.println("applicationValues : " + applicationValues);
		}
	}

	/**
	 * Stores values into ArrayList
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void storeLoanDetails(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException {
		System.out.println("******************** storeLoanDetails **********************");
		String colHeader = "";
		String rowValue = "";
		columnNames = new ArrayList<String>();
		applicationValues = new ArrayList<String>();
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		String borrowerInfo = getElementByUsing(args[0]).getText().trim();
		List<WebElement> webList = driver.findElements(By.xpath(args[1]));
		columnNames.add("Loan Program");
		columnNames.add("Purpose of Loan");
		columnNames.add("Status");
		columnNames.add("Branch Code");
		columnNames.add("Application Id");
		columnNames.add("Borrower Name");
		applicationValues.add(selectedLoanProgram);
		applicationValues.add(purposeOfLoan);
		applicationValues.add(args[3]);
		if (borrowerInfo.length() > 1) {
			applicationValues.add(branchCode);
			applicationValues.add(applicationId);
			applicationValues.add(borrowerName);
		}
		for (int i = 1; i <= webList.size(); i++) {
			List<WebElement> headerAllValues = driver.findElements(By.xpath(args[1] + "[" + i + args[2]));
			for (int j = 1; j <= headerAllValues.size(); j++) {
				if ((j % 2) != 0) {
					colHeader = driver.findElement(By.xpath(args[1] + "[" + i + args[2] + "[" + j + "]")).getText()
							.replace(":", "").trim();
					if (!(colHeader.equalsIgnoreCase("Loan Program") || colHeader.equalsIgnoreCase("Purpose of Loan")
							|| colHeader.equalsIgnoreCase("Status"))) {
						columnNames.add(colHeader);
					}
				} else {
					rowValue = driver.findElement(By.xpath(args[1] + "[" + i + args[2] + "[" + j + "]")).getText();
					if (rowValue.contains(",")) {
						rowValue = rowValue.replace(",", "");
					}
					if (!(colHeader.equalsIgnoreCase("Loan Program") || colHeader.equalsIgnoreCase("Purpose of Loan")
							|| colHeader.equalsIgnoreCase("Status"))) {
						applicationValues.add(rowValue);
					}
				}
			}
		}
	}

	/**
	 * Search dynamically generated application id
	 * 
	 * @author muni.reddy
	 * @param values
	 * @return
	 * @throws Exception
	 */
	public void searchApplication(String values) throws Exception {
		System.out.println("************ searchApplication ************ " + applicationId + branchCode);
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String args[] = KWVariables.getVariables().get(values).split(":");
		waitForElement(getElementByUsing(args[0]), (elementDisplayTimeOut * 2));
		if (args[3].equalsIgnoreCase("underwritter")) {
			Thread.sleep(4000);
			getElementByUsing(args[0]).sendKeys(branchCode);
			getElementByUsing(args[1]).clear();
			getElementByUsing(args[1]).sendKeys(applicationId);
			getElementByUsing(args[2]).click();
			Thread.sleep(1000);
		} else {
			getElementByUsing(args[0]).clear();
			getElementByUsing(args[0]).sendKeys(applicationId);
			Thread.sleep(1000);
			getElementByUsing(args[1]).click();
			Thread.sleep(1000);
			unlockApplication(args[4]);
			waitForElement(getElementByUsing(args[3]), (elementDisplayTimeOut * 4));
		}
	}

	/**
	 * Gets current window handle
	 * 
	 * @param args
	 * @throws TwfException
	 */
	public void getCurrentWindowhandle(String args) throws TwfException {
		System.out.println("getCurrentWindowhandle ********** ");
		driver = DriverFactory.getDriver();
		parenthandle = driver.getWindowHandle();
	}

	/**
	 * Switches to parent window
	 * 
	 * @param values
	 * @throws InterruptedException
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public void switchToParentWindow(String values)
			throws InterruptedException, BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("************ switchToParentWindow *********");
		String pHandle;
		driver = DriverFactory.getDriver();
		pHandle = parenthandle;
		driver.switchTo().window(pHandle);
		Thread.sleep(500);
	}

	/**
	 * Get date based on the input date format and the number of days
	 * 
	 * @author muni.reddy
	 * @param dateFormat
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws BiffException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public String enterClosingDate(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************* enterClosingDate *******************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		String emplymentStartDate = Util.getDateToSearch(args[0], args[1], Integer.parseInt(args[2]));

		if (args.length > 4) {
			String val[] = emplymentStartDate.split("/");
			if (args[4].equalsIgnoreCase("dd")) {
				emplymentStartDate = val[1];
			} else if (args[4].equalsIgnoreCase("mm")) {
				emplymentStartDate = val[0];
			} else if (args[4].equalsIgnoreCase("yyyy")) {
				emplymentStartDate = val[2];
			} else if (args[4].equalsIgnoreCase("mmyyyy")) {
				emplymentStartDate = val[0] + val[2];
			}
		}
		emplymentStartDate = emplymentStartDate.trim();

		if (getElementByUsing(args[3]).isDisplayed()) {
			getElementByUsing(args[3]).clear();
			Thread.sleep(500);
			getElementByUsing(args[3]).sendKeys(emplymentStartDate);
			Thread.sleep(1000);
		}
		return emplymentStartDate;
	}

	/**
	 * Get date based on the format and the numbers of days requested by adding
	 * subtracting
	 * 
	 * @author muni.reddy
	 * @param strDateFormat
	 * @param days
	 * @return
	 * @throws TwfException
	 * @throws IOException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 */
	public void enterFirstPaymentDate(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("************* enterFirstPaymentDate *******************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(args[0]);
		String firstPaymnetStartDate = formatter.format(date.with(TemporalAdjusters.firstDayOfNextMonth()));
		getElementByUsing(args[3]).clear();
		getElementByUsing(args[3]).sendKeys(firstPaymnetStartDate);
	}

	/**
	 * Select Loan Program and handle alert
	 * 
	 * @param dataPoolArgs
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */
	public void selectLoanProgramAndHandleAlert(Map<String, String> dataPoolArgs)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("*************** selectLoanProgramAndHandleAlert ****************");
		driver = DriverFactory.getDriver();
		waitForElement(getElementByUsing(saveAndContinueBtn), (elementDisplayTimeOut * 2));
		String optionToSelect = dataPoolArgs.get(loanProgramd_dd);
		loanProgramId = optionToSelect;
		Select ddOptions = new Select(getElementByUsing(loanProgramd_dd));
		try {
			ddOptions.selectByValue(optionToSelect);
			Thread.sleep(1000);
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("No Alert!!!!!!!!!!");
		}
	}

	/**
	 * Select drop-down option by visible text
	 * 
	 * @param val
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public void selectOptions(String val) throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("***************** selectOptions *******************");
		String args[] = KWVariables.getVariables().get(val).split(":");
		Select opt = new Select(getElementByUsing(args[0]));
		opt.selectByVisibleText(args[1]);
	}

	/**
	 * Store drop-down options for future comparison
	 * 
	 * @param val
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public void storeSelectedOption(String val)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("***************** storeSelectedOption *******************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(val).split(":");
		Select opt = new Select(getElementByUsing(args[0]));
		purposeOfLoan = opt.getFirstSelectedOption().getText().trim();
		if (driver.findElements(By.xpath(args[1])).size() > 0) {
			if (driver.findElement(By.xpath(args[1])).isDisplayed()) {
				driver.findElement(By.xpath(args[1])).click();
			}
		}
	}

	/**
	 * Click on take action link
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	/*
	 * /html/body/center/form[1]/table[2]/tbody/tr: ]/td[4]: ]/td[5]/a:
	 * ]/td[22]/a :UVA_UA_DisplayAll_btn
	 * 
	 * 
	 */

	public void clickOnTakeActionLink(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("********************clickOnTakeActionLink********************");
		driver = DriverFactory.getDriver();

		String args[] = KWVariables.getVariables().get(values).split(":");
		// getElementByUsing(args[3]).click();
		Thread.sleep(3000);
		List<WebElement> aList = driver.findElements(By.xpath(args[0]));
		int actualBranchCode = 0;
		int actualApplicationId = 0;
		for (int i = 2; i < (aList.size()); i++) {
			actualBranchCode = Integer
					.parseInt(driver.findElement(By.xpath(args[0] + "[" + i + args[1])).getText().trim());
			actualApplicationId = Integer
					.parseInt(driver.findElement(By.xpath(args[0] + "[" + i + args[2])).getText().trim());
			if (Integer.parseInt(branchCode) == actualBranchCode
					&& Integer.parseInt(applicationId) == actualApplicationId) {
				driver.findElement(By.xpath(args[0] + "[" + i + args[3])).click();
				break;
			}

		}
	}

	/**
	 * Select multiple radio buttons
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void selectMultipleRadioButtons(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("******************** selectMultipleRadioButtons *****************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		List<WebElement> aList = driver.findElements(By.xpath(args[0]));
		int i = Integer.parseInt(args[4]);
		String attributeValue = "";
		if (!loanProgramId.equalsIgnoreCase("879")) {
			for (; i < aList.size(); i++) {
				attributeValue = driver.findElement(By.xpath(args[0] + "[" + i + "]")).getAttribute(args[3]).trim();
				if (!attributeValue.equalsIgnoreCase("text")) {
					driver.findElement(By.xpath(args[0] + "[" + i + args[1] + args[2])).click();
				}
			}
		}
	}

	/**
	 * Creates loans CSV file
	 * 
	 * @author muni.reddy
	 * @param args
	 * @throws IOException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws TwfException
	 */
	public void createLoansCSVFile(String args)
			throws IOException, BiffException, InvalidFormatException, TwfException {
		System.out.println("************* createLoansCSVFile *************");
		String[] fileToCreateDetails = KWVariables.getVariables().get(args).split(":");

		FileWriter writer = null;
		String resultFolderDate = new SimpleDateFormat(fileToCreateDetails[0]).format(new Date())
				.replaceAll("[\\/\\-\\:]", "_");
		File currentResDir = new File(System.getProperty("user.dir") + File.separator + fileToCreateDetails[1]
				+ File.separator + fileToCreateDetails[2] + resultFolderDate);
		currentResDir.mkdirs();
		String newLoansCreatedCSVFilePath = currentResDir.getAbsolutePath() + File.separator + fileToCreateDetails[3];

		try {
			boolean alreadyExists = new File(newLoansCreatedCSVFilePath).exists();
			if (!alreadyExists) {
				writer = new FileWriter(newLoansCreatedCSVFilePath, true);
				for (int i = 0; i < columnNames.size(); i++) {
					writer.write(columnNames.get(i).toString());
					writer.write(",");
				}
				writer.write("\n");

			} else {
				writer = new FileWriter(newLoansCreatedCSVFilePath, true);
			}
			for (int i = 0; i < applicationValues.size(); i++) {
				writer.write(applicationValues.get(i).toString().trim());
				writer.write(",");
			}
			writer.write("\n");
			System.out.println("Write success!");
		} catch (IOException e) {
			e.printStackTrace();
			writer.close();
		} finally {
			writer.close();
		}
	}

	/**
	 * Click on Continue button
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void clickOnContinueButton(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("***************** clickOnContinueButton *************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		if (loanProgramId != null && loanProgramId.length() > 1 && !args[0].equalsIgnoreCase("Underwriting")) {
			if (loanProgramId.equalsIgnoreCase("879") || loanProgramId.equalsIgnoreCase("2159")) {
				getElementByUsing(args[0]).click();
				waitForElement(getElementByUsing(args[1]), (elementDisplayTimeOut * 2));
			}
		}
	}

	/**
	 * Verify application status on history page
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyStatusInAppHistory(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("******************* verifyStatusInAppHistory *****************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		waitForElement(getElementByUsing(args[1]), elementDisplayTimeOut);
		boolean appStatus = false;
		String actualAppStatus = "";
		String expectedStatus = args[2].toUpperCase();
		for (int i = 1; i < driver.findElements(By.xpath(args[3])).size(); i++) {
			actualAppStatus = driver.findElement(By.xpath(args[3] + "[" + i + args[4])).getText().trim().toUpperCase();
			if (actualAppStatus.contains(expectedStatus)) {
				appStatus = true;
				break;
			}
		}

		if (!appStatus) {
			Util.addExceptionToReport("App History page status mismatch", actualAppStatus, expectedStatus);
		}
	}

	/**
	 * Verify RespaDate
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyRespaDate(String values) throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("**************** verifyRespaDate *****************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		String actualRrespaDate = getElementByUsing(args[0]).getText().trim();
		if (args[1].equalsIgnoreCase("RespaDate") && actualRrespaDate.length() < 4) {
			Util.addExceptionToReport("Respa date not created", actualRrespaDate, "Should have respa date");
		} else if (args[1].equalsIgnoreCase("NoRespaDate") && actualRrespaDate.length() > 4) {
			Util.addExceptionToReport("Respa date created", actualRrespaDate, "Should not have respa date");
		}
	}

	/**
	 * Gets the appraiser details
	 * 
	 * @param args
	 * @return
	 * @throws IOException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws TwfException
	 * @throws ParseException
	 * @throws InterruptedException
	 */
	public String getAppraiserDetails(String args) throws IOException, BiffException, InvalidFormatException,
			TwfException, ParseException, InterruptedException {
		System.out.println("******** getAppraiserDetails **********");
		String[] values = KWVariables.getVariables().get(args).split(":");
		Date dateToday = new Date();
		List<WebElement> ColumnDataA = DriverFactory.getDriver().findElements(By.xpath(values[0] + values[2]));
		List<WebElement> ColumnDataB = DriverFactory.getDriver().findElements(By.xpath(values[0] + values[3]));
		List<WebElement> ColumnDataC = DriverFactory.getDriver().findElements(By.xpath(values[0] + values[1]));
		if (ColumnDataA.size() > 0) {
			for (int i = 0; i < ColumnDataA.size(); i++) {
				String columnValueA = ColumnDataA.get(i).getText().trim();
				String columnValueB = ColumnDataB.get(i).getText().trim();
				Date dateA = new SimpleDateFormat(values[4]).parse(columnValueA);
				Date dateB = new SimpleDateFormat(values[4]).parse(columnValueB);
				if (dateA.after(dateToday) && dateB.after(dateToday)) {
					appraisalValue = ColumnDataC.get(i).getText().trim();
					DriverFactory.getDriver().findElement(By.xpath(values[0] + "[" + (i + 1) + "]" + values[1]))
							.click();
					break;
				}
			}
		}
		return appraisalValue;
	}

	/**
	 * Select Purchase radio button
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void selectPurchaseRadioButton(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("***************** selectPurchaseRadioButton ************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		if (!args[0].equalsIgnoreCase(selectedLoanProgram)) {
			if (args[1].equalsIgnoreCase(purposeOfLoan)) {
				try {
					getElementByUsing(args[2]).click();
				} catch (Exception e) {
					getElementByUsing(args[3]).click();
				}
			}
		}
	}

	/**
	 * Selects subject property
	 * 
	 * @param dataPoolArgs
	 * @throws Exception
	 */
	public void selectSubjectProperty(Map<String, String> dataPoolArgs) throws Exception {
		System.out.println("*************** selectLoanProgramAndHandleAlert ****************");
		driver = DriverFactory.getDriver();
		String optionToSelect = dataPoolArgs.get(purposeOfLoan_dd);
		scrollToElement(reoSubjectProperty_No_rbtn);
		if (optionToSelect.equalsIgnoreCase(purchase_val)) {
			getElementByUsing(reoSubjectProperty_No_rbtn).click(); // No
		} else {
			getElementByUsing(reoSubjectProperty_Yes_rbtn).click(); // Yes
		}
	}

	/**
	 * Gets ratios from web page and enters
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */

	/*
	 * //tbody//tr[48]//td[6] :MyK_Header_housingRatio :MyK_Header_dtiRatio
	 * :UVA_HousingRatio_AUS_txb :UVA_DTIRatio_AUS_txb
	 */

	public void getRatiosAndEnter(String values)
			throws TwfException, BiffException, IOException, InvalidFormatException, InterruptedException {
		System.out.println("************** getRatiosAndEnter ************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		if (driver.findElements(By.xpath(args[0])).size() > 0) {
			String housingRatio = getElementByUsing(args[1]).getText();
			String dtiRatio = getElementByUsing(args[2]).getText();
			housingRatio = housingRatio.substring(housingRatio.lastIndexOf(":") + 1, housingRatio.length()).trim();
			dtiRatio = dtiRatio.substring(dtiRatio.lastIndexOf(":") + 1, dtiRatio.length()).trim();
			getElementByUsing(args[3]).clear();
			getElementByUsing(args[3]).sendKeys(housingRatio);
			getElementByUsing(args[4]).clear();
			getElementByUsing(args[4]).sendKeys(dtiRatio);
			Thread.sleep(100);
		}
	}

	/**
	 * Selects submitting to Submit to Underwriting options
	 * 
	 * @param dataPoolArgs
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void selectSubmitToUnderwritingRadioButton(Map<String, String> dataPoolArgs)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("***************** selectSubmitToUnderwritingRadioButton *************");
		// Get data from Data pool column "RepoElements"
		String submitToUnderwritingType = dataPoolArgs.get(submitToUnderwriting);
		if (submitToUnderwritingType != null && submitToUnderwritingType.length() > 1) {
			if (submitToUnderwritingType.equalsIgnoreCase(credit_val)) {
				getElementByUsing(suCreditRtbn).click();
			} else if (submitToUnderwritingType.equalsIgnoreCase(property_val)) {
				getElementByUsing(suPPropertyRbtn).click();
			} else if (submitToUnderwritingType.equalsIgnoreCase(full_val)) {
				getElementByUsing(suFullRbtn).click();
			}
		}
	}

	/**
	 * Select radio button
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void selectRadioButton(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("*************** selectRadioButton ***********");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		if (driver.findElements(By.xpath(args[0])).size() != 0) {
			WebElement rbtn = getElementByUsing(args[1]);
			if (rbtn.isDisplayed())
				rbtn.click();

			if (!rbtn.isSelected())
				Util.addExceptionToReport("Radio buttton is not clicked", "Radio button should be selected",
						"Radio button is not selected");
		}
	}

	/**
	 * @author rahul.kunjumon, this method is to handle any popups coming in the
	 *         application.
	 * @param val
	 * @return
	 * @throws Exception
	 */
	public String unlockApplication(String val) throws Exception {
		System.out.println("****************** unlockApplication ***********************");
		WebDriver driver = DriverFactory.getDriver();
		String[] value = val.split(",");
		getElementByUsing(value[1]).click();
		Thread.sleep(1000);
		try {
			Alert alert = driver.switchTo().alert();
			Thread.sleep(1000);
			String alertMessage = alert.getText();
			if (value[0].equalsIgnoreCase("OK")) {
				alert.accept();
				// Store the current window handle
				String winHandleBefore = driver.getWindowHandle();
				Thread.sleep(1000);
				// click on unlock app button on the page
				getElementByUsing(value[2]).click();
				// switch to new window
				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
				}
				// click on unlock app button on the new page
				getElementByUsing(value[3]).click();
				// Switch back to original browser (first window)
				driver.switchTo().window(winHandleBefore);
				getElementByUsing(value[1]).click();
			}
			if (value[0].equalsIgnoreCase("CANCEL")) {
				alert.dismiss();
			}
			return (alertMessage);
		} catch (Exception e) {
			return "No alert found";
		}
	}
	/*
	 * MyK_Continue_BT ,MYK_unlockApp_btn ,MYK_unlockApp_popupbtn
	 */

	public void unlockApplicationIfLocked(String val) throws Exception {
		System.out.println("****************** unlockApplicationIfLocked ***********************");
		WebDriver driver = DriverFactory.getDriver();

		String value[] = KWVariables.getVariables().get(val).split(",");
		driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		getElementByUsing(value[0]).click();
		Thread.sleep(1000);
		try {
			Set<String> s1 = driver.getWindowHandles();
			Iterator<String> i1 = s1.iterator();
			while (i1.hasNext()) {
				String windowUniqueId = i1.next();
				if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
					driver.switchTo().window(windowUniqueId);
					Thread.sleep(2000);
					getElementByUsing(value[2]).click();
					Thread.sleep(2000);
				}
			}
			driver.switchTo().window(mainWindow);
			getElementByUsing(value[0]).click();
		} catch (Exception e) {
			System.out.println("No alert");
		}
	}

	/**
	 * Stores the selected loan program
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */
	public void storeLoanProgram(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("*************** storeLoanProgram ****************");
		String args = KWVariables.getVariables().get(values);
		driver = DriverFactory.getDriver();
		Select ddOptions = new Select(getElementByUsing(args));
		selectedLoanProgram = ddOptions.getFirstSelectedOption().getText().trim();
		selectedLoanProgram = selectedLoanProgram.replace("  ", "");
	}

	/**
	 * Get values for data pool methods
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void getValuesForDataPoolsMethods(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("************************ getValuesForDataPoolsMethods *******************");
		String dpValues[] = KWVariables.getVariables().get(values).split(":");
		ssn1_txb = dpValues[0];
		ssn2_txb = dpValues[1];
		ssn3_txb = dpValues[2];
		homePhoneNumber1_txb = dpValues[3];
		saveAndContinueBtn = dpValues[4];
		loanProgramd_dd = dpValues[5];
		submitToUnderwriting = dpValues[6];
		suCreditRtbn = dpValues[7];
		suPPropertyRbtn = dpValues[8];
		suFullRbtn = dpValues[9];
		purposeOfLoan_dd = dpValues[10];
		WebDriver driver = DriverFactory.getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		reoSubjectProperty_No_rbtn = dpValues[11];
		reoSubjectProperty_Yes_rbtn = dpValues[12];
		credit_val = dpValues[13];
		property_val = dpValues[14];
		full_val = dpValues[15];
		purchase_val = dpValues[16];
		CBssn1_txb = dpValues[17];
		CBssn2_txb = dpValues[18];
		CBssn3_txb = dpValues[19];
		homePhoneNumber2_txb = dpValues[20];
		homePhoneNumber3_txb = dpValues[21];
		cb_HomePhoneNumber1_txb = dpValues[22];
		cb_HomePhoneNumber2_txb = dpValues[23];
		cb_HomePhoneNumber3_txb = dpValues[24];
	}

	/**
	 * Enters SSN number and handles the alert
	 * 
	 * @param dataPoolArgs
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void handleSSNAlert(Map<String, String> dataPoolArgs) throws BiffException, IOException, TwfException {
		System.out.println("****************** handleSSNAlert ********************");
		driver = DriverFactory.getDriver();
		String ssn1 = dataPoolArgs.get(ssn1_txb);
		String ssn2 = dataPoolArgs.get(ssn2_txb);
		String ssn3 = dataPoolArgs.get(ssn3_txb);
		String homePhoneNumber1 = dataPoolArgs.get(homePhoneNumber1_txb);
		String homePhoneNumber2 = dataPoolArgs.get(homePhoneNumber2_txb);
		String homePhoneNumber3 = dataPoolArgs.get(homePhoneNumber3_txb);

		getElementByUsing(ssn1_txb).sendKeys(ssn1);
		getElementByUsing(ssn2_txb).sendKeys(ssn2);
		getElementByUsing(ssn3_txb).sendKeys(ssn3);
		try {

			getElementByUsing(homePhoneNumber1_txb).sendKeys(homePhoneNumber1);
			Thread.sleep(1500);
			Alert alert = driver.switchTo().alert();
			alert.accept();
			// Thread.sleep(500);

		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
		try {
			getElementByUsing(homePhoneNumber2_txb).sendKeys(homePhoneNumber2);
			Thread.sleep(500);
			Alert alert = driver.switchTo().alert();
			alert.accept();

		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
		try {
			getElementByUsing(homePhoneNumber3_txb).sendKeys(homePhoneNumber3);
			Thread.sleep(500);
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
	}

	/**
	 * Verifies the element displayed status
	 * 
	 * @param Values
	 * @throws Exception
	 */
	public void checkIfIsDisplayed(String Values) throws Exception {
		System.out.println("****************** checkIfIsDisplayed ********************");
		String args[] = Values.split(",");
		if (getElementByUsing(args[0]).isDisplayed()) {
			getElementByUsing(args[0]).clear();
			getElementByUsing(args[0]).sendKeys(args[1]);
		}
	}

	/**
	 * Enter value to Simultaneous text box
	 * 
	 * @param dataPoolArgs
	 * @throws Exception
	 */
	public void enterValueToSimultaneousTxb(Map<String, String> dataPoolArgs) throws Exception {
		System.out.println("****************** enterValueToSimultaneousTxb ********************");
		String value = dataPoolArgs.get("MyK_NRF_SimultaneousIssue_txb");
		if (getElementByUsing("MyK_NRF_SimultaneousIssue_txb").isDisplayed()) {
			getElementByUsing("MyK_NRF_SimultaneousIssue_txb").clear();
			getElementByUsing("MyK_NRF_SimultaneousIssue_txb").sendKeys(value);
		}
	}

	/**
	 * Selects Seller information check box
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void sellerInformation(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** sellerInformation ********************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(args[0]).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		waitForElement(getElementByUsing(args[1]), (elementDisplayTimeOut * 4));
		getElementByUsing(args[1]).click();
		Thread.sleep(1000);
		getElementByUsing(args[2]).click();
		driver.close();
		driver.switchTo().window(winHandleBefore);
	}

	/**
	 * Clicks on radio button
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void checkRadioButton(String values) throws Exception {
		System.out.println("****************** checkRadioButton ********************");
		String args = KWVariables.getVariables().get(values);
		if (getElementByUsing(args).isDisplayed()) {
			getElementByUsing(args).click();
		}
	}

	/**
	 * Enters the underwriter credit scores
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InvalidFormatException
	 */
	public void enterUnderwritterCreditScores(String values)
			throws TwfException, BiffException, IOException, InvalidFormatException {
		System.out.println("************** enterUnderwritterCreditScores ************");
		String args[] = KWVariables.getVariables().get(values).split(",");
		String repoElements[] = args[0].split(":");
		String val[] = args[1].split(":");
		driver = DriverFactory.getDriver();
		if (driver.findElements(By.xpath(repoElements[0])).size() > 0) {
			if (driver.findElement(By.name(repoElements[1])).isEnabled()
					&& driver.findElement(By.name(repoElements[1])).isDisplayed()) {
				getElementByUsing(repoElements[2]).sendKeys(val[0]);
				getElementByUsing(repoElements[3]).sendKeys(val[0]);
				getElementByUsing(repoElements[4]).sendKeys(val[0]);
				getElementByUsing(repoElements[5]).sendKeys(val[0]);
			}
			if (driver.findElement(By.name(repoElements[6])).isEnabled()
					&& driver.findElement(By.name(repoElements[6])).isDisplayed()) {
				getElementByUsing(repoElements[7]).sendKeys(val[1]);
				getElementByUsing(repoElements[8]).sendKeys(val[2]);
			}
		}
		if (driver.findElements(By.xpath(repoElements[9])).size() > 0) {
			if (driver.findElement(By.xpath(repoElements[9])).isDisplayed()) {
				getElementByUsing(repoElements[10]).click();

			}
		}
	}

	/**
	 * Verifies the element displayed status and clicks on it
	 * 
	 * @param Values
	 * @throws Exception
	 */
	public void checkIfIsDisplayedAndClick(String Values) throws Exception {
		System.out.println("****************** checkIfIsDisplayedAndClick ********************");
		String args[] = Values.split(",");
		if (getElementByUsing(args[0]).isDisplayed()) {
			getElementByUsing(args[0]).click();
		}
	}

	public void selectCheckBox(String val) throws TwfException, BiffException, InterruptedException, IOException {
		System.out.println("****************** selectCheckBox ********************");
		Util.selectCheckBoxOption(getElementByUsing(val));
	}

	/**
	 * Waits for the element to disappear
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void waitUntilElementInvisibility(String values) throws Exception {
		System.out.println("****************** waitUntilElementInvisibility ********************");
		String[] args = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(args[0]));
		wait.until(ExpectedConditions.invisibilityOfElementWithText(By.id(args[1]), args[2]));
	}

	/**
	 * Verifies the application submitted without application edits
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyAppSubmittedWithoutApplicationEdits(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("**************** verifyAppSubmittedWithoutApplicationEdits *****************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		if (driver.findElements(By.xpath(args[0])).size() > 2) {
			String firstEditMessage = getElementByUsing(args[1]).getText().trim();
			// Purpose of Loan: Construction to Perm
			if (!purposeOfLoan.equalsIgnoreCase(args[3]) && firstEditMessage.contains(args[2])) {
				Util.addExceptionToReport("There are application edits", firstEditMessage,
						"There should not be any Errors in application edits");
			}
		}
	}

	/**
	 * Enters the seller information
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void enterSellerInformation(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** enterSellerInformation ********************");
		String repoElements[] = KWVariables.getVariables().get(values).split("::")[0].split(":");
		String val[] = KWVariables.getVariables().get(values).split("::")[1].split(":");
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(repoElements[0]).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		waitForElement(getElementByUsing(repoElements[1]), (elementDisplayTimeOut * 4));
		getElementByUsing(repoElements[1]).click();
		Thread.sleep(1000);

		if (getElementByUsing(repoElements[2]).isEnabled()) {
			getElementByUsing(repoElements[2]).clear();
			getElementByUsing(repoElements[2]).sendKeys(val[0]);
			getElementByUsing(repoElements[3]).clear();
			getElementByUsing(repoElements[3]).sendKeys(val[1]);
			getElementByUsing(repoElements[4]).clear();
			getElementByUsing(repoElements[4]).sendKeys(val[2]);
			new Select(getElementByUsing(repoElements[5])).selectByValue(val[3]);
			getElementByUsing(repoElements[6]).clear();
			getElementByUsing(repoElements[6]).sendKeys(val[4]);
			Thread.sleep(1000);
		}
		getElementByUsing(repoElements[7]).click();
		Thread.sleep(1000);
		driver.close();
		driver.switchTo().window(winHandleBefore);
	}

	/**
	 * Selects radio button
	 * 
	 * @param Values
	 * @throws Exception
	 */
	public void selectanyRadioButton(String Values) throws Exception {
		System.out.println("****************** selectanyRadioButton ********************");
		String args[] = Values.split(",");
		driver = DriverFactory.getDriver();
		List<WebElement> radioType = driver.findElements(By.name(args[0]));
		for (WebElement e : radioType) {
			if (e.getAttribute("value").equalsIgnoreCase(args[1])) {
				e.click();
			}
		}
	}

	/**
	 * Enters the SSN number for co-borrower
	 * 
	 * @param dataPoolArgs
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void handleSSNAlertForCoBor(Map<String, String> dataPoolArgs)
			throws BiffException, IOException, TwfException {
		System.out.println("****************** handleSSNAlertForCoBor ********************");
		driver = DriverFactory.getDriver();
		String ssn1 = dataPoolArgs.get(CBssn1_txb);
		String ssn2 = dataPoolArgs.get(CBssn2_txb);
		String ssn3 = dataPoolArgs.get(CBssn3_txb);
		String cb1_phone = dataPoolArgs.get(cb_HomePhoneNumber1_txb);
		String cb2_phone = dataPoolArgs.get(cb_HomePhoneNumber2_txb);
		String cb3_phone = dataPoolArgs.get(cb_HomePhoneNumber3_txb);
		getElementByUsing(CBssn1_txb).sendKeys(ssn1);
		getElementByUsing(CBssn2_txb).clear();
		getElementByUsing(CBssn2_txb).sendKeys(ssn2);
		// try {
		getElementByUsing(CBssn3_txb).clear();
		getElementByUsing(CBssn3_txb).sendKeys(ssn3);
		/*
		 * getElementByUsing("MyK_CoBorrowerHomePhone1").clear();
		 * Thread.sleep(2000); Alert alert = driver.switchTo().alert();
		 * alert.accept(); } catch (Exception e) {
		 * System.out.println("No alert found !!!!!!!!"); }
		 */
		try {

			getElementByUsing(cb_HomePhoneNumber1_txb).sendKeys(cb1_phone);
			Thread.sleep(1500);
			Alert alert = driver.switchTo().alert();
			alert.accept();
			// Thread.sleep(500);

		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
		try {
			getElementByUsing(cb_HomePhoneNumber2_txb).sendKeys(cb2_phone);
			Thread.sleep(500);
			Alert alert = driver.switchTo().alert();
			alert.accept();

		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
		try {
			getElementByUsing(cb_HomePhoneNumber3_txb).sendKeys(cb3_phone);
			Thread.sleep(500);
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
	}

	/**
	 * Clears the input box values
	 * 
	 * @param Values
	 * @throws Exception
	 */
	public void clearTextBox(String Values) throws Exception {
		System.out.println("****************** clearTextBox ********************");
		getElementByUsing(Values).clear();
	}

	/**
	 * @author rahul.kunjumon, this method is to handle any popups coming in the
	 *         application.
	 * @param val
	 * @return
	 * @throws Exception
	 */
	public String popupHandler(String val) throws Exception {
		System.out.println("***************** popupHandler **********************");
		WebDriver driver = DriverFactory.getDriver();
		String[] value = val.split(",");
		scrollToElement(value[1]);
		getElementByUsing(value[1]).click();
		Thread.sleep(2000);
		try {
			Alert alert = driver.switchTo().alert();
			Thread.sleep(2000);
			String alertMessage = alert.getText().trim();
			if (value[0].equalsIgnoreCase("OK")) {
				alert.accept();
			}
			if (value[0].equalsIgnoreCase("CANCEL")) {
				alert.dismiss();
			}
			return (alertMessage);
		} catch (Exception e) {
			return "No alert found";
		}
	}

	/**
	 * @author rahul.kunjumon,An extension of popupHandler method, this method
	 *         compares the alert message with the given text
	 * @param val
	 * @throws Exception
	 */
	public void popupHandlerVerify(String val) throws Exception {
		String[] value = val.split(";");
		String ActualValue = popupHandler(value[0]);
		if (!value[1].contains(ActualValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
	}

	/**
	 * @author rahul.kunjumon, this method is get the selected option from the
	 *         drop-down and compare it with the value passed
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 *             value
	 */
	public void compareSelectedOption(String val) throws Exception, IOException, TwfException {
		String[] value = val.split(",");
		Select dropdown = new Select(getElementByUsing(value[0]));
		String ActualValue = dropdown.getFirstSelectedOption().getAttribute(value[2]);
		if (!value[1].equals(ActualValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
	}

	/**
	 * @author rahul.kunjumon, this method captures the selected option and
	 *         stores it in a static variable
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void getSelectedOption(String val) throws Exception, IOException, TwfException {
		Select dropdown = new Select(getElementByUsing(val));
		selectedDDOption = dropdown.getFirstSelectedOption().getText();
		selectedOptionValue = dropdown.getFirstSelectedOption().getAttribute("value").trim();
	}

	/**
	 * Enters values for borrower address
	 * 
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void enterValuesForBorrowerAddress(String val) throws Exception, IOException, TwfException {
		String[] args = KWVariables.getVariables().get(val).split(",");
		driver = DriverFactory.getDriver();
		for (int i = 3; i <= 10; i++) {
			driver.findElement(By.xpath(args[0] + i + args[1])).click();
			driver.findElement(By.xpath(args[0] + i + args[2])).clear();
			driver.findElement(By.xpath(args[0] + i + args[2])).sendKeys(args[6]);
			driver.findElement(By.xpath(args[0] + i + args[3])).clear();
			driver.findElement(By.xpath(args[0] + i + args[3])).sendKeys(args[7]);
			driver.findElement(By.xpath(args[0] + i + args[4])).click();
			Thread.sleep(500);
		}
		getElementByUsing(args[5]).click();
	}

	/**
	 * @author rahul.kunjumon, This method is to check if an element is visible
	 *         or not.
	 * @param val
	 * @throws Exception
	 */
	public void verifyElementVisibility(String val) throws Exception {
		String valuesPassed[] = val.split(",");
		if (valuesPassed[1].equalsIgnoreCase(visible)) {
			if (getElementByUsing(valuesPassed[0]).isDisplayed()) {
				Util.addExceptionToReport("Element Present", "Actual present", "Expected Absence");
			}
		} else if (valuesPassed[1].equalsIgnoreCase(notVisible)) {
			if (!getElementByUsing(valuesPassed[0]).isDisplayed()) {
				Util.addExceptionToReport("Element Present", "Actual present", "Expected Absence");
			}
		}
	}

	/**
	 * Verifies the PDF
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void checkForPdfFile(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** checkForPdfFile ********************");
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(values).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String currentUrl = driver.getCurrentUrl();
		Thread.sleep(1000);
		if (currentUrl.contains(".pdf") || currentUrl.contains(".PDF") || currentUrl.contains("IncomeDebtWorksheet")) {
			System.out.println("pdf Generated successfully");
		} else {
			Util.addExceptionToReport("pdf not generated", "Actual:not generated", "Expected: to be generated");
		}
		driver.close();
		driver.switchTo().window(winHandleBefore);
	}

	/**
	 * Scrolls to the web element
	 * 
	 * @param ele
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void scrollToElement(String ele) throws Exception, IOException, TwfException {
		System.out.println("************** scrollToElement **********************");
		WebDriver driver = DriverFactory.getDriver();
		WebElement ele2 = getElementByUsing(ele);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(4000,0)", ele2);
		js.executeScript("window.scrollBy(4000,0)", ele2);
		js.executeScript("arguments[0].scrollIntoView(true);", ele2);
	}

	/**
	 * Verifies the check box for monthly income
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyChekboxForMonthlyIncome(String values) throws Exception, IOException, TwfException {
		System.out.println("----------------verifyChekboxForMonthlyIncome--------------");
		System.out.println("-----------VALUES----------" + values);
		WebDriver driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(values).split(",");
		int startValue = Integer.parseInt(args[2]);
		int endValue = Integer.parseInt(args[3]);
		for (int i = startValue; i <= endValue; i++) {
			WebElement checkBoxElement = driver.findElement(By.xpath(args[0] + i + args[1]));
			System.out.println("--------checkBoxElement.isSelected()----" + checkBoxElement.isSelected());
			if (!checkBoxElement.isSelected()) {
				Util.addExceptionToReport("CheckBox Not Selected", "Actual not present", "Expected To be Selected");
			}
		}
	}

	/**
	 * Verify Monthly Income Additional Fields
	 * 
	 * @param ele
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyMonthlyIncomeAdditionalFields(String ele) throws Exception, IOException, TwfException {
		String args[] = ele.split(",");
		WebDriver driver = DriverFactory.getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		for (int i = 2; i <= 9; i++) {
			WebElement checkBoxElement = driver.findElement(By.name(args[0] + i + args[1]));
			if (!checkBoxElement.isDisplayed()) {
				Util.addExceptionToReport("Elemenyt Not Displayed", "Actual not present", "Expected To be displayed");
			}
		}
	}

	public void navigateToPreviousPage(String ele) throws Exception, IOException, TwfException {
		System.out.println("---------------navigateToPreviousPage-----------");
		WebDriver driver = DriverFactory.getDriver();
		driver.navigate().back();

	}

	/**
	 * Verify Settings Page values
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */

	public void verifySettingsPage(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("****************** verifySettingsPage ********************");
		driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(values).split(",");
		String headerName = args[4] + args[5];
		if (!getElementByUsing(args[0]).getText().equalsIgnoreCase(headerName)) {
			Util.addExceptionToReport("value doesnot Matches", getElementByUsing(args[0]).getText(), headerName);
		}
		if (!getElementByUsing(args[1]).getText().equalsIgnoreCase(args[5])) {
			Util.addExceptionToReport("value doesnot Matches", getElementByUsing(args[1]).getText(), args[5]);
		}
		if (!getElementByUsing(args[2]).getText().equalsIgnoreCase(args[6])) {
			Util.addExceptionToReport("value doesnot Matches", getElementByUsing(args[2]).getText(), args[6]);
		}
		if (!getElementByUsing(args[3]).isDisplayed()) {
			Util.addExceptionToReport("Elemenyt Not Displayed", "Actual not present", "Expected To be displayed");
		}
		getElementByUsing(args[3]).clear();
		getElementByUsing(args[3]).sendKeys(args[7]);
		getElementByUsing(args[8]).click();

	}

	/**
	 * Verifies edit my account functionality
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */
	public void verifyEditMyAccount(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("****************** verifyEditMyAccount ********************" + values);
		String[] args = KWVariables.getVariables().get(values).split(",");
		if (!getElementByUsing(args[0]).getText().equalsIgnoreCase(args[4])) {
			Util.addExceptionToReport("value doesnot Matches", getElementByUsing(args[0]).getText(), args[4]);
		}
		if (!getElementByUsing(args[1]).getAttribute("value").equalsIgnoreCase(args[5])) {
			Util.addExceptionToReport("value doesnot Matches", getElementByUsing(args[1]).getAttribute("value"),
					args[5]);
		}
		if (!getElementByUsing(args[2]).getAttribute("value").equalsIgnoreCase(args[6])) {
			Util.addExceptionToReport("value doesnot Matches", getElementByUsing(args[2]).getAttribute("value"),
					args[6]);
		}
		if (!getElementByUsing(args[3]).getAttribute("value").equalsIgnoreCase(args[7])) {
			Util.addExceptionToReport("value doesnot Matches", getElementByUsing(args[3]).getAttribute("value"),
					args[7]);
		}
		if (!getElementByUsing(args[9]).isSelected()) {
			Util.addExceptionToReport("CheckBox Not Selected", "Actual not present", "Expected To be Selected");
		}
		getElementByUsing(args[1]).clear();
		getElementByUsing(args[1]).sendKeys(args[5]);
		getElementByUsing(args[2]).clear();
		getElementByUsing(args[2]).sendKeys(args[6]);
		getElementByUsing(args[3]).clear();
		getElementByUsing(args[3]).sendKeys(args[7]);
		getElementByUsing(args[8]).click();
	}

	/**
	 * Handle Save And Continue button alert
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void handleSaveAndContinueBtnAlert(String values) throws BiffException, IOException, TwfException {
		System.out.println("****************** handleSaveAndContinueBtnAlert ********************");
		driver = DriverFactory.getDriver();
		try {
			getElementByUsing(values).click();
			Thread.sleep(500);
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
	}

	/**
	 * @author rahul.kunjumon, This method is to update the Income information
	 *         in the 1003 page and verify it
	 * @param values
	 * @throws Exception
	 */
	public void verifyUpdateIncome(String values) throws Exception {
		System.out.println("******************* verifyUpdateIncome ***********************"+values);
		String args[] = KWVariables.getVariables().get(values).split(":");
		String val[] = args[0].split(",");
		String val1[] = args[1].split(",");
		for (int i = 0; i < val.length; i++) {
			String savedIncomeDetails = getElementByUsing(val[i]).getAttribute(args[2]);
			String enteredIncomeDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(val1[i]);
			if (savedIncomeDetails.contains(".00")) {
				savedIncomeDetails = savedIncomeDetails.substring(0, (savedIncomeDetails.indexOf(".")));
			}
			if (enteredIncomeDetails.contains(",")) {
				enteredIncomeDetails = enteredIncomeDetails.replace(",", "");
			}
			if (savedIncomeDetails.contains(",")) {
				savedIncomeDetails = savedIncomeDetails.replace(",", "");
			}
			if (enteredIncomeDetails.contains("/")) {
				enteredIncomeDetails = enteredIncomeDetails.substring(0, (enteredIncomeDetails.indexOf("/")));
			}
			if (enteredIncomeDetails.contains(".00")) {
				enteredIncomeDetails = enteredIncomeDetails.substring(0, (enteredIncomeDetails.indexOf(".")));
			}
			if (!enteredIncomeDetails.equals(savedIncomeDetails)) {
				Util.addExceptionToReport("Income Details mismatch", enteredIncomeDetails + "",
						savedIncomeDetails + "");
			}
		}
	}

	/**
	 * Verify Liabilities Table Data
	 * 
	 * @param values
	 * @throws Exception
	 */

	public void verifyLiabilitiesTableData(String values) throws Exception {
		System.out.println("--------------verifyLiabilitiesTableData--------------");
		String args[] = KWVariables.getVariables().get(values).split(",");
		WebDriver driver = DriverFactory.getDriver();
		String value;
		try{
			boolean ele= driver.findElement(By.xpath(args[7] + 1 + "]/a")).isDisplayed();
			value=args[7];
			
		}catch (Exception e) {
			boolean ele= driver.findElement(By.xpath(args[8] + 1 + "]/a")).isDisplayed();
			value=args[8];
		}
			
		System.out.println("-------------value------------"+value);
		for (int i = 1; i <= 6; i++) {
			String storedDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[i]);
			if (i == 1) {
				WebElement TableElement = driver.findElement(By.xpath(value + i + "]/a"));
				if (!TableElement.getText().trim().equalsIgnoreCase(storedDetails)) {
					Util.addExceptionToReport("Details mismatch", TableElement.getText().trim(), storedDetails);
				}
			} else if (i == 2) {
				WebElement TableElement = driver.findElement(By.xpath(value + i + "]"));
				if (!TableElement.getText().trim().equalsIgnoreCase(selectedDDOption)) {
					Util.addExceptionToReport("Details mismatch", TableElement.getText().trim(), selectedDDOption);
				}
			} else {
				WebElement TableElement = driver.findElement(By.xpath(value + i + "]"));
				String savedIncomeDetails = TableElement.getText().trim();
				if (savedIncomeDetails.contains(".")) {
					savedIncomeDetails = savedIncomeDetails.substring(0, (savedIncomeDetails.indexOf(".")));
				}
				if (!savedIncomeDetails.equalsIgnoreCase(storedDetails)) {
					Util.addExceptionToReport("Details mismatch", savedIncomeDetails, storedDetails);
				}
			}
		}
	}

	/**
	 * Verifies the closing history page
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void checkForClosingHistoryPage(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** checkForClosingHistoryPage ********************");
		String args[] = values.split(";");
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(args[0]).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String currentUrl = getElementByUsing(args[1]).getText();
		Thread.sleep(1000);
		if (!currentUrl.equalsIgnoreCase(args[2])) {
			Util.addExceptionToReport("page not found", currentUrl, args[2]);
		}
		driver.close();
		driver.switchTo().window(winHandleBefore);
	}

	/**
	 * Verifies the attribute value
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyElementAttribute(String values) throws Exception {
		System.out.println("****************** verifyElementAttribute ********************" + values);
		String[] args = KWVariables.getVariables().get(values).split(",");
		String StoredValue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[1]).trim();
		String actualValue = getElementByUsing(args[0]).getAttribute(args[2]).trim();
		WebDriver driver = DriverFactory.getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		if (actualValue.contains(".00")) {
			actualValue = actualValue.substring(0, (actualValue.indexOf(".")));
		}
		if (StoredValue.contains(".00")) {
			StoredValue = StoredValue.substring(0, (StoredValue.indexOf(".")));
		}
		if (!actualValue.equalsIgnoreCase(StoredValue)) {
			Util.addExceptionToReport("Text Mismatch", actualValue, StoredValue);
		}
	}

	/**
	 * Verifies the attribute value
	 * 
	 * @param values
	 * @throws Exception
	 */

	public void validateLockScreenElementAttributes(String values) throws Exception {
		System.out.println("****************** validateLockScreenElementAttributes ********************" + values);
		String[] val = KWVariables.getVariables().get(values).split(":");
		boolean isValueMismatch = false;
		String storedValue = "";
		String actualValue = "";
		String actualReportValue = "";
		String expectedReportValue = "";
		getElementByUsing(val[0].split(",")[0]).click();
		Thread.sleep(2000);

		if (!getElementByUsing(val[0].split(",")[1]).getText().contains(val[0].split(",")[2])) {
			for (int i = 1; i < val.length; i++) {
				String[] args = val[i].split(",");
				storedValue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[1]).trim();
				actualValue = getElementByUsing(args[0]).getAttribute(args[2]).trim();
				if (actualValue.contains(".00")) {
					actualValue = actualValue.substring(0, (actualValue.indexOf(".")));
				}
				if (storedValue.contains(".00")) {
					storedValue = storedValue.substring(0, (storedValue.indexOf(".")));
				}
				if (!actualValue.equalsIgnoreCase(storedValue)) {
					expectedReportValue = expectedReportValue + "\n" + storedValue;
					actualReportValue = actualReportValue + "\n" + actualValue;
					isValueMismatch = true;
				}
			}
		} else {
			System.out.println("Lock-In not allowed:  Pricing not allowed - beyond the cutoff time of 7:30 p.m");
			getElementByUsing(val[4].split(",")[0]).click();
		}

		if (isValueMismatch) {
			Util.addExceptionToReport("Values mismatch:", actualReportValue, expectedReportValue);
		}
		getElementByUsing(val[4].split(",")[1]).click();
	}

	/**
	 * Verifies the attribute value
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyMultipleElementAttribute(String values) throws Exception {
		System.out.println("****************** verifyMultipleElementAttribute ********************" + values);
		String[] args = KWVariables.getVariables().get(values).split(":");
		String reponame[] = args[0].split(",");
		String value[] = args[1].split(",");
		for (int i = 0; i < value.length; i++) {
			String savedValue = getElementByUsing(reponame[i]).getAttribute(args[2]);
			if (savedValue.contains(".")) {
				savedValue = savedValue.substring(0, (savedValue.indexOf(".")));
			}
			if (!savedValue.equals(value[i])) {
				Util.addExceptionToReport("Text Mismatch", savedValue, value[i]);
			}
		}
	}

	/**
	 * Verifies the group page drop-down
	 * 
	 * @param values
	 * @throws Exception
	 */

	/*
	 * 1st Mortgage (Including 1st w/piggyback 2nds) ,Stand Alone Closed End 2nd
	 * ,Stand Alone Heloc ,Copy existing app to this one
	 * :/html/body/center/form[1]/table[2]/tbody/tr[,]/td[2],3,6
	 * 
	 * 
	 */
	public void verifyGroupPageDD(String values) throws Exception {
		System.out.println("****************** verifyGroupPageDD ********************" + values);
		String[] value = KWVariables.getVariables().get(values).split(":");
		String[] SearchParam = value[0].split(",");
		String[] codeParam = value[1].split(",");
		int startValue = Integer.parseInt(codeParam[2]);
		int endValue = Integer.parseInt(codeParam[3]);
		WebDriver driver = DriverFactory.getDriver();
		try {
			for (int i = startValue; i <= endValue; i++) {

				if (!(i == 15 || i == 16)) {
					WebElement TableElement = driver.findElement(By.xpath(codeParam[0] + i + codeParam[1]));
					if (!TableElement.getText().trim().equalsIgnoreCase(SearchParam[i - startValue])) {
						Util.addExceptionToReport("Details mismatch", TableElement.getText().trim(),
								SearchParam[i - startValue]);
					}
				}
			}
		} catch (Exception e) {
			System.out.println("***********catch***********");
			String[] newcodeParam = value[2].split(",");
			for (int i = startValue; i <= endValue; i++) {

				if (!(i == 15 || i == 16)) {
					WebElement TableElement = driver.findElement(By.xpath(newcodeParam[0] + i + newcodeParam[1]));
					if (!TableElement.getText().trim().contains(SearchParam[i - startValue])) {
						Util.addExceptionToReport("Details mismatch", TableElement.getText().trim(),
								SearchParam[i - startValue]);
					}
				}
			}

		}

	}

	/**
	 * Verifies the New Application fields
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyNewApplicationFields(String values) throws Exception {
		String args[] = KWVariables.getVariables().get(values).split(",");
		WebDriver driver = DriverFactory.getDriver();
		try {
			for (int i = 1; i <= 17; i++) {
				if (i != 3) {
					WebElement TableElement = driver.findElement(By.xpath(args[18] + i + "]"));
					if (!TableElement.getAttribute(args[17]).equalsIgnoreCase(args[i - 1])) {
						Util.addExceptionToReport("Details mismatch", TableElement.getAttribute(args[17]).trim(),
								args[i - 1]);
					}
				}
			}
		} catch (Exception e) {
			for (int i = 1; i <= 17; i++) {
				if (i != 3) {
					WebElement TableElement = driver.findElement(By.xpath(args[19] + i + "]"));
					if (!TableElement.getAttribute(args[17]).equalsIgnoreCase(args[i - 1])) {
						Util.addExceptionToReport("Details mismatch", TableElement.getAttribute(args[17]).trim(),
								args[i - 1]);
					}
				}
			}
		}
	}

	/**
	 * Verifies if the check-box is selected
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyIfCheckBoxSelected(String values) throws Exception, IOException, TwfException {
		System.out.println("************ verifyIfCheckBoxSelected **********");
		String valuesPassed[] = values.split(",");
		if (valuesPassed[1].equalsIgnoreCase(selected)) {
			if (getElementByUsing(valuesPassed[0]).isSelected()) {
				Util.addExceptionToReport("Element not selected", "not selected", "selected");
			}
		} else if (valuesPassed[1].equalsIgnoreCase(notselected)) {
			if (!getElementByUsing(valuesPassed[0]).isSelected()) {
				Util.addExceptionToReport("Element Present", "selected", "not selected");
			}
		}
	}

	/**
	 * Searches for name
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void performNameSearch(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** performNameSearch ********************");
		String parameter[] = KWVariables.getVariables().get(values).split(":");
		String args[] = parameter[0].split(";");
		String value[] = parameter[1].split(";");
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(args[0]).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		getElementByUsing(args[1]).clear();
		getElementByUsing(args[1]).sendKeys(value[0]);
		getElementByUsing(args[2]).clear();
		getElementByUsing(args[2]).sendKeys(value[1]);
		getElementByUsing(args[3]).click();
		Thread.sleep(1000);
		getElementByUsing(args[4]).click();
		driver.switchTo().window(winHandleBefore);
	}

	/**
	 * Verifies the closing Date Tracking
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void closingDateTracking(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** closingDateTracking ********************");
		String parameter[] = KWVariables.getVariables().get(values).split(":");
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(parameter[3]).click();
		try {
			Thread.sleep(2000);
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			Select oSelect = new Select(getElementByUsing(parameter[1]));
			oSelect.selectByIndex(1);
			getElementByUsing(parameter[2]).sendKeys(parameter[4]);
			Thread.sleep(1000);
			getElementByUsing(parameter[5]).click();
			driver.switchTo().window(winHandleBefore);
		} catch (Exception e) {
		}
	}

	/**
	 * Verifies the element is editable or not
	 * 
	 * @param values
	 * @throws Exception
	 */

	public void verifyEditableOrNot(String values) throws Exception {
		System.out.println("****************** verifyEditableOrNot ********************" + values);
		String[] codeParam = KWVariables.getVariables().get(values).split(",");
		int startValue = Integer.parseInt(codeParam[2]);
		int endValue = Integer.parseInt(codeParam[3]);
		WebDriver driver = DriverFactory.getDriver();
		for (int i = startValue; i <= endValue; i++) {
			WebElement TableElement = driver.findElement(By.xpath(codeParam[0] + i + codeParam[1]));
			String tagName = TableElement.getTagName();
			if (tagName.equalsIgnoreCase(codeParam[4])) {
				Util.addExceptionToReport("Value is a text field", "Present", "Not Present");
			}
		}
	}

	/**
	 * Switches to the child window and switches back to its parent window
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */

	/*
	 * MyKey_MyLoans_LoanCompPage, MyKey_MyLoans_LoanCompHeader,
	 * MyK_CreditReport_Exit_btn, Loan Cost Analysis
	 */

	public void switchToWindowAndSwitchBack(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		System.out.println("********************* switchToWindowAndSwitchBack ***************************");
		String args[] = KWVariables.getVariables().get(values).split(",");
		driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		waitForElement(getElementByUsing(args[0]), 10);
		getElementByUsing(args[0]).click();
		Thread.sleep(2000);

		// To handle all new opened window.
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(2000);
				if (!getElementByUsing(args[1]).getText().equalsIgnoreCase(args[3])) {
					Util.addExceptionToReport("Text Mismatch", getElementByUsing(args[1]).getText(), args[3]);
				}
				Thread.sleep(1000);
				getElementByUsing(args[2]).click();
			}
		}
		driver.switchTo().window(mainWindow);
		Thread.sleep(3000);
	}

	/**
	 * Switches to the child window and switches back to its parent window
	 * 
	 * @param values
	 * @throws Exception 
	 */
	public void switchToWindowAndSwitchBackToParent(String values)
			throws Exception {
		System.out.println("********************* switchToWindowAndSwitchBackToParent ***************************"+ values);
		driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		String[] args = KWVariables.getVariables().get(values).split(":");
		getElementByUsing(args[0]).click();
		Thread.sleep(2000);
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(2000);
				scrollToElement(args[1]);
				getElementByUsing(args[1]).click();
				Thread.sleep(2000);
			}
		}
		driver.switchTo().window(mainWindow);
	}

	/**
	 * Selects check boxes on declarations page
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void selectCheckBoxesForDeclaration(String values) throws Exception, IOException, TwfException {
		System.out.println("********************* click if the field is displayed ***************************");
		String valuesPassed[] = KWVariables.getVariables().get(values).split(",");
		driver = DriverFactory.getDriver();
		if (driver.findElements(By.xpath(valuesPassed[0])).size() != 0
				&& driver.findElement(By.xpath(valuesPassed[0])).isDisplayed()) {
			for (int i = 1; i < valuesPassed.length; i++) {
				getElementByUsing(valuesPassed[i]).click();
			}
		}
	}

	/**
	 * Click on Exit button
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void clickExitButton(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		System.out.println("********************* clickExitButton ***************************");
		driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		waitForElement(getElementByUsing(values), 10);
		getElementByUsing(values).click();
		Thread.sleep(1000);
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(1000);
			}
		}
	}

	/**
	 * Click on element if its present
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void selectButtonIfPresent(String values) throws Exception, IOException, TwfException {
		System.out.println("********************* selectButtonIfPresent ***************************" + values);
		driver = DriverFactory.getDriver();
		if (driver.findElements(By.xpath(values)).size() != 0) {
			driver.findElement(By.xpath(values)).click();
		}
	}

	/**
	 * Verifies the myloans columns
	 * 
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void validateMyLoansColumns(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("********************* validateMyLoansColumns ***************************" + val);
		String[] value = KWVariables.getVariables().get(val).split(",");
		List<WebElement> columnData = DriverFactory.getDriver().findElements(By.xpath(value[0]));
		String enteredDetails = "";
		if (columnData.size() > 0) {
			for (int j = 0; j < columnData.size(); j++) {
				String actualValue = columnData.get(j).getText().trim();
				if (!value[1].contains("var_")) {
					enteredDetails = selectedOptionValue;
				} else {
					enteredDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[1]);
				}
				if (!actualValue.contains(enteredDetails)) {
					Util.addExceptionToReport("Filtering of loans failed", actualValue, enteredDetails);

				}
			}
		}
	}

	/**
	 * Perform tab out keyboard event
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void performTabOut(String values) throws Exception, IOException, TwfException {
		System.out.println("*********************** performTabOut ******************************");
		driver = DriverFactory.getDriver();
		getElementByUsing(values).sendKeys(Keys.TAB);
	}

	/**
	 * Verifies Loan Tracking Check boxes
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyLoanTrackingCheckboxes(String values) throws Exception, IOException, TwfException {
		System.out.println("********************* verifyLoanTrackingCheckboxes ***************************" + values);
		driver = DriverFactory.getDriver();
		String[] value = KWVariables.getVariables().get(values).split(",");
		List<WebElement> ErrorData = DriverFactory.getDriver().findElements(By.xpath(value[0]));
		List<WebElement> columnData = DriverFactory.getDriver().findElements(By.xpath(value[1]));
		if (ErrorData.size() > 0) {
			System.out.println(ErrorData.get(0).getText());
		} else if (columnData.size() > 0) {
			System.out.println("actualSize :" + columnData.size());
		} else {
			Util.addExceptionToReport("Filtering of loans failed", "No values displayed", "Values to be displayed");
		}
	}

	/**
	 * Verifies the filters functionalities
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void verifyTheFilter(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("*********************** verifyTheFilter ******************************");
		String valuePassed[] = KWVariables.getVariables().get(values).split(",");
		String textbox = getElementByUsing(valuePassed[0]).getAttribute("value");
		ArrayList<String> list = new ArrayList<String>();
		List<WebElement> filter = DriverFactory.getDriver().findElements(By.xpath(valuePassed[1]));
		Thread.sleep(3000);
		for (WebElement allElement : filter) {
			String element = allElement.getText();
			list.add(element);
		}
		int count = list.size();
		for (int i = 1; i < count - 1; i++) {
			if (!list.get(i).contains(textbox))
				Util.addExceptionToReport("Search textbox and records in Pipeline does not match", list.get(i),
						textbox);
		}
	}

	/**
	 * verifyTextAttribute
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyTextAttribute(String values) throws Exception {
		System.out.println("****************** verifyTextAttribute ********************" + values);
		String[] args = KWVariables.getVariables().get(values).split(",");
		String StoredValue = getElementByUsing(args[0]).getText().trim();
		String actualValue = getElementByUsing(args[1]).getText().trim();
		if (actualValue.contains(".00")) {
			actualValue = actualValue.substring(0, (actualValue.indexOf(".")));
		}
		if (actualValue.equals("0")) {
			actualValue = "";
		}
		if (StoredValue.contains(".00")) {
			StoredValue = StoredValue.substring(0, (StoredValue.indexOf(".")));
		}
		if (!actualValue.contains(StoredValue)) {
			System.out.println("------------actualValue--------" + actualValue);
			System.out.println("------------StoredValue--------" + StoredValue);
			Util.addExceptionToReport("Text Mismatch", actualValue, StoredValue);
		}
	}

	/**
	 * compareDropDownText
	 * 
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void compareDropDownText(String val) throws Exception, IOException, TwfException {
		System.out.println("****************** compareDropDownText ********************" + val);
		String[] value = val.split(",");
		Select dropdown = new Select(getElementByUsing(value[0]));
		String ActualValue = dropdown.getFirstSelectedOption().getText().trim().toUpperCase();
		String expectedValue = getElementByUsing(value[1]).getText().trim().toUpperCase();
		if (ActualValue.contains("-")) {
			ActualValue = ActualValue.substring(0, (ActualValue.indexOf("-"))).trim();
		}
		if (!ActualValue.contains(expectedValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, expectedValue);
		}
	}

	/**
	 * ifCheckboxSlectedClick
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void ifCheckboxSlectedClick(String values) throws BiffException, IOException, TwfException {
		System.out.println("--------------checkbox------------");
		WebElement Checkbox = getElementByUsing(values);
		try {
			if (Checkbox.isSelected())
				Checkbox.click();
		} catch (Exception e) {
			System.out.println("Checkbox is already selected");
		}
	}

	/**
	 * verifyMonthlyIncomeValue
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyMonthlyIncomeValue(String values) throws Exception {
		System.out.println("********************* verifyMonthlyIncomeValue ***************************");
		String args[] = KWVariables.getVariables().get(values).split(",");
		int TotalValue = Integer.parseInt(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[0]));
		int ValueA = Integer.parseInt(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[1]));
		int ValueB = Integer.parseInt(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[2]));
		int ExpectedValue = ValueA + ValueB;
		if (!(TotalValue == (ExpectedValue))) {
			Util.addExceptionToReport("Details mismatch", Integer.toString(TotalValue),
					Integer.toString(ExpectedValue));
		}
	}

	/**
	 * Verifies the ratios
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 */
	public void verifyRatio(String values) throws TwfException, BiffException, IOException {
		System.out.println("****************** Verify Ratio ********************");
		String ratio = getElementByUsing(values).getText();
		if (ratio.equals(".000/.000"))
			Util.addExceptionToReport("Search textbox and records in Pipeline doesnt match", ratio,
					"ratio should not be zero");
	}

	/**
	 * Verifies the elements display status
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void isElementDisplayed(String values) throws BiffException, IOException, TwfException {
		getElementByUsing(values).isDisplayed();
		if (!getElementByUsing(values).isDisplayed()) {
			Util.addExceptionToReport("The element is not displayed on the webpage", "Element is not displayed",
					"Element displayed");
		}
	}

	/**
	 * Verifies the comment saved
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyCommentIsAdded(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		String valuePassed[] = KWVariables.getVariables().get(values).split(",");
		String StoredValue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(valuePassed[1]);
		ArrayList<String> list = new ArrayList<String>();
		List<WebElement> filter = DriverFactory.getDriver().findElements(By.xpath(valuePassed[0]));
		Thread.sleep(1000);
		for (WebElement allElement : filter) {
			String element = allElement.getText();
			list.add(element);
		}
		for (String commentTextarea : list) {
			if (!StoredValue.contains(StoredValue)) {
				Util.addExceptionToReport("Search textbox and records in Pipeline doesnt match", commentTextarea,
						StoredValue);
			}
		}
	}

	/**
	 * Verifies the Cancel Deny alert function
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws BiffException
	 * @throws IOException
	 */
	public void verifyCancelDenyAlert(String values)
			throws TwfException, InterruptedException, BiffException, IOException {
		System.out.println("****************** verifyCancelDenyAlert ********************");
		WebDriver driver = DriverFactory.getDriver();
		getElementByUsing(values).click();
		Thread.sleep(2000);
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	/**
	 * Enter Application id and search
	 * 
	 * @param value
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */
	public void toEnterAppIdIntoAppTextbox(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("******************** getBranchAndApplicationNumber **********************");
		applicationValues = new ArrayList<String>();
		driver = DriverFactory.getDriver();
		String arguments[] = KWVariables.getVariables().get(value).split(",");
		String borrowerInfo = getElementByUsing(arguments[0]).getText().replace("   ", " ").trim();
		if (borrowerInfo.length() > 1) {
			String loanInfo[] = borrowerInfo.split(" ");
			branchCode = loanInfo[1].split("-")[0];
			applicationId = loanInfo[1].split("-")[1];
		}
		getElementByUsing(arguments[1]).click();
		Thread.sleep(2000);
		getElementByUsing(arguments[2]).clear();
		getElementByUsing(arguments[2]).sendKeys(applicationId);
	}

	/**
	 * Verify government monitoring page contents
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyTheGovtMontitoring(String values) throws Exception {
		String[] value = KWVariables.getVariables().get(values).split("!!");
		String[] SearchParam = value[0].split(",");
		String[] codeParam = value[1].split(",");
		int startValue = Integer.parseInt(codeParam[2]);
		int endValue = Integer.parseInt(codeParam[3]);
		WebDriver driver = DriverFactory.getDriver();
		WebElement tableElement = null;

		for (int i = startValue; i <= endValue; i++) {
			if (!(i == 7)) {
				tableElement = driver.findElement(By.xpath(codeParam[0] + i + codeParam[1]));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(4000,0)", tableElement);
				js.executeScript("window.scrollBy(4000,0)", tableElement);
				js.executeScript("arguments[0].scrollIntoView(true);", tableElement);

				if (!tableElement.getText().trim().equalsIgnoreCase(SearchParam[i - startValue])) {
					Util.addExceptionToReport("Details mismatch", tableElement.getText().trim(),
							SearchParam[i - startValue]);
				}
			}
		}
	}

	public void verifyApplicationEdits(String values) throws Exception {
		System.out.println("********************* verifyApplicationEdits *******************");
		String[] value = KWVariables.getVariables().get(values).split("::");
		String[] SearchParam = value[0].split(",");
		String[] codeParam = value[1].split(",");
		String[] newcodeParam = value[2].split(",");
		WebDriver driver = DriverFactory.getDriver();
		String warningTest = "";
		WebElement tableElement = null;
		boolean isErrorMsgMatch = false;
		String expecteEdit = "";
		try {
			System.out.println("**********************try*************************");
			int noOfWarning_Errors = driver.findElements(By.xpath(codeParam[0])).size() - 1;
			for (int j = 0; j < SearchParam.length; j++) {
				expecteEdit = SearchParam[j];
				for (int i = 2; i < noOfWarning_Errors; i++) {
					tableElement = driver.findElement(By.xpath(codeParam[0] + codeParam[1] + i + codeParam[2]));
					warningTest = tableElement.getText().trim();
					if (!warningTest.contains("Warning")) {

						if (tableElement.getText().trim().equalsIgnoreCase(expecteEdit)) {
							isErrorMsgMatch = true;
							break;
						} else {
							System.out.println(" Value NOT matched ???? ");
						}
					}
				}
			}
			if (!isErrorMsgMatch) {
				Util.addExceptionToReport("Details mismatch", warningTest, expecteEdit);
			}
		} catch (Exception e) {

			System.out.println("**********************catch*************************");
			int noOfWarning_Errors = driver.findElements(By.xpath(newcodeParam[0])).size() - 1;
			for (int j = 0; j < SearchParam.length; j++) {
				expecteEdit = SearchParam[j];
				for (int i = 2; i < noOfWarning_Errors; i++) {
					tableElement = driver
							.findElement(By.xpath(newcodeParam[0] + newcodeParam[1] + i + newcodeParam[2]));
					warningTest = tableElement.getText().trim();
					if (!warningTest.contains("Warning")) {

						if (tableElement.getText().trim().equalsIgnoreCase(expecteEdit)) {
							isErrorMsgMatch = true;
							break;
						} else {
							System.out.println(" Value NOT matched ???? ");
						}
					}
				}
			}
			if (!isErrorMsgMatch) {
				Util.addExceptionToReport("Details mismatch", warningTest, expecteEdit);
			}
		}
	}

	/*
	 * Warning,Error:Seller name is missing.
	 * 
	 * ::/html/body/center/form[1]/table/tbody/tr,[,]/td[2]
	 * 
	 * ::/html/body/div[3]/center/form/table/tbody/tr,[,]/td[2]
	 * 
	 * 
	 */
	public void clickOnApproriateError(String values) throws Exception {
		System.out.println("********************* clickOnApproriateError *******************");
		String[] value = KWVariables.getVariables().get(values).split("::");
		String[] searchParam = value[0].split(",");
		String[] codeParam = value[1].split(",");
		String[] newcodeParam = value[2].split(",");
		WebDriver driver = DriverFactory.getDriver();
		String warningTest = "";
		WebElement tableElement = null;
		try {
			System.out.println("----------------try------------------");
			int noOfWarning_Errors = driver.findElements(By.xpath(codeParam[0])).size() - 1;
			for (int i = 1; i < noOfWarning_Errors; i++) {
				tableElement = driver.findElement(By.xpath(codeParam[0] + codeParam[1] + i + codeParam[2]));
				warningTest = tableElement.getText().trim();

				if (!warningTest.contains(searchParam[0])) {
					if (warningTest.equalsIgnoreCase(searchParam[1])) {
						tableElement.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("----------------catch------------------");
			int noOfWarning_Errors = driver.findElements(By.xpath(newcodeParam[0])).size() - 1;
			for (int i = 1; i < noOfWarning_Errors; i++) {
				tableElement = driver.findElement(By.xpath(newcodeParam[0] + newcodeParam[1] + i + newcodeParam[2]));
				warningTest = tableElement.getText().trim();
				if (!warningTest.contains(searchParam[0])) {
					if (warningTest.contains(searchParam[1])) {
						tableElement.click();
						break;
					}
				}
			}

		}
	}

	/**
	 * Verify pending Conditions
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyPendingCondition(String values) throws Exception {
		System.out.println("code is executing-------------------->");
		String[] value = KWVariables.getVariables().get(values).split("::");
		String[] SearchParam = value[0].split(":");
		String[] codeParam = value[1].split(",");
		int startValue = Integer.parseInt(codeParam[2]);
		int endValue = Integer.parseInt(codeParam[3]);
		WebDriver driver = DriverFactory.getDriver();
		for (int i = startValue; i <= endValue; i++) {
			WebElement TableElement = driver.findElement(By.xpath(codeParam[0] + i + codeParam[1]));
			if (!TableElement.getText().trim().contains(SearchParam[i - startValue])) {
				Util.addExceptionToReport("Details mismatch", TableElement.getText().trim(),
						SearchParam[i - startValue]);
			}
		}
	}

	/**
	 * Verify check-box selection status
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyCheckboxIsSelected(String values) throws BiffException, IOException, TwfException {
		System.out.println("---------------verifyCheckboxIsSelected----------" + values);
		WebElement KeepMeLogIN_Checkbox = getElementByUsing(values);
		if (!KeepMeLogIN_Checkbox.isSelected()) {
			Util.addExceptionToReport("Checkbox is not selected", "expected to be selected", "actually not selected");
		}
	}

	/**
	 * Verify selected drop-down option
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifySelectedDropDown(String values) throws BiffException, IOException, TwfException {
		System.out.println("<---------------------- Verifying the drop down------------------->");
		String[] value = values.split(":");
		String expectedValue = value[1].trim();
		Select drpCountry = new Select(getElementByUsing(value[0]));
		String selectedValueInDropDown = drpCountry.getFirstSelectedOption().getText().trim();
		if (!selectedValueInDropDown.equalsIgnoreCase(expectedValue))
			Util.addExceptionToReport("Mismatch", selectedValueInDropDown, value[1]);
	}

	/**
	 * Verify the page title
	 * 
	 * @param values
	 * @throws InterruptedException
	 * @throws TwfException
	 */
	public void verifyTheTitle(String values) throws InterruptedException, TwfException {
		System.out.println(" ------------- verifyTheTitle-------------");
		driver = DriverFactory.getDriver();
		Thread.sleep(3000);
		String actualTitle = driver.getTitle();
		if (!actualTitle.equalsIgnoreCase(values))
			Util.addExceptionToReport("Mismatch in the page", actualTitle, values);
	}

	/**
	 * Add borrower groups
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */

	/*
	 * MyKey_Application_FirstName_txtbox ,MyKey_Application_LastName_txtbox
	 * ,MyKey_Borrower_Page__Previous_btn
	 * ,/html/body/center/form[1]/table/tbody/tr[5]/td[1]/table/tbody/tr[3]/td[2
	 * ]/select/option
	 */

	public void addBorrowerGroup(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		String[] value = KWVariables.getVariables().get(values).split(",");
		String firstName = getElementByUsing(value[0]).getAttribute("value");
		String lastName = getElementByUsing(value[1]).getAttribute("value");
		String fullName = lastName + ", " + firstName;
		getElementByUsing(value[2]).click();
		Thread.sleep(2000);
		ArrayList<String> list = new ArrayList<String>();
		List<WebElement> allSuggestions = DriverFactory.getDriver().findElements(By.xpath(value[3]));
		for (WebElement suggestion : allSuggestions) {
			String element = suggestion.getText();
			list.add(element);
		}
		boolean found = false;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equalsIgnoreCase(fullName)) {
				found = true;
			}
		}
		if (found == false)
			Util.addExceptionToReport("Added borrower is not found", "Borrower name is not found in Borrower drop down",
					"Borrower name is found in Borrower drop down");
	}

	/**
	 * Verify unlock application
	 * 
	 * @param args
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */

	public void verifyUnlockApplication(String args)
			throws BiffException, IOException, TwfException, InterruptedException {
		System.out.println("********************* verifyUnlockApplication ***************************");
		String[] value = args.split(",");
		driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		getElementByUsing(value[0]).click();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(2000);
				getElementByUsing(value[1]).click();
				Thread.sleep(2000);
			}
		}
		driver.switchTo().window(mainWindow);
		getElementByUsing(value[0]).click();
		String pageText = getElementByUsing(value[5]).getText();
		if (!pageText.contains(value[3]))
			Util.addExceptionToReport("Borrwer Information should be dispalyed",
					"Borrwer Information should be dispalyed", "Borrwer Information is not displayed");
		getElementByUsing(value[2]).click();
		getElementByUsing(value[4]).click();
		Thread.sleep(2000);
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(2000);
				if (getElementByUsing(value[1]).isDisplayed())
					Util.addExceptionToReport("The unlock button should not be displayed",
							"The unlock button is displayed", "The unlock button should not be displayed");
			}
		}
		driver.switchTo().window(mainWindow);
	}

	/**
	 * Verify the text on Popup
	 * 
	 * @param val
	 * @throws Exception
	 */
	public void verifyTheTextInPopup(String val) throws Exception {
		System.out.println("***************** verifyTheTextInPopup ****************");
		String[] value = val.split(";");
		String ActualValue = popupHandler(value[0]);
		Thread.sleep(2000);
		if (!ActualValue.contains(value[1])) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
	}

	/**
	 * Enter the Application number
	 * 
	 * @param val
	 * @throws Exception
	 */
	public void enterTheApplicationNumber(String val) throws Exception {
		System.out.println("****************** enterTheApplicationNumber ********************");
		getElementByUsing(val).clear();
		getElementByUsing(val).sendKeys(applicationId);
	}

	/**
	 * Verify the copied seller1 information into seller2
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void verifySeller(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** verifySeller ********************");
		String params = KWVariables.getVariables().get(values);
		String[] value = params.split(",");
		String SellerAddress1 = getElementByUsing(value[0]).getAttribute("value");
		String SellerCity1 = getElementByUsing(value[1]).getAttribute("value");
		String SellerZip1 = getElementByUsing(value[2]).getAttribute("value");
		String SellerAddress2 = getElementByUsing(value[3]).getAttribute("value");
		String SellerCity2 = getElementByUsing(value[4]).getAttribute("value");
		String SellerZip2 = getElementByUsing(value[5]).getAttribute("value");
		if (!(SellerAddress1.equalsIgnoreCase(SellerAddress2)) && (SellerCity1.equalsIgnoreCase(SellerCity2))
				&& (SellerZip1.equalsIgnoreCase(SellerZip2)))
			Util.addExceptionToReport("Mismatch is value", " value mismatch",
					"Seller1 information is copied to information2");
	}

	/**
	 * Validation property information
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */

	public void validationPropertyInformation(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("************** validationPropertyInformation ************");
		String[] value = values.split(",");
		String address = getElementByUsing(value[1]).getText();
		String loanPurpose = getElementByUsing(value[2]).getText();
		String addressTxtbox = getElementByUsing(value[0]).getAttribute("value");
		Select select = new Select(getElementByUsing(value[3]));
		WebElement option = select.getFirstSelectedOption();
		String dropDown = option.getText();

		if (!addressTxtbox.contains(address)) {
			Util.addExceptionToReport("Mismatch is value", address, addressTxtbox);
		}

		if (!loanPurpose.equalsIgnoreCase(dropDown)) {
			Util.addExceptionToReport("Mismatch is value", loanPurpose, dropDown);
		}
	}

	/**
	 * switches to child window and switches back to its parent window
	 * 
	 * @param args
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void switchBackToPreviousWindow(String args)
			throws BiffException, IOException, TwfException, InterruptedException {
		System.out.println("********************* switchBackToPreviousWindow ***************************");
		String[] value = args.split(",");
		driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		getElementByUsing(value[0]).click();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(2000);
				String Title = getElementByUsing(value[1]).getText();
				if (Title.equalsIgnoreCase(value[2]))
					getElementByUsing(value[3]).click();
			}
		}
		driver.switchTo().window(mainWindow);
	}

	/**
	 * Verify clicking on Exit button
	 * 
	 * @param args
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void clickOnExitBtn(String args) throws BiffException, IOException, TwfException, InterruptedException {
		System.out.println("********************* clickOnExitBtn ***************************");
		String[] value = args.split(",");
		driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		getElementByUsing(value[0]).click();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(2000);
				String Title = getElementByUsing(value[1]).getText();
				if (Title.equalsIgnoreCase(value[2])) {
					getElementByUsing(value[3]).click();
					Thread.sleep(3000);
				}
				getElementByUsing(value[4]).click();
			}
		}
		driver.switchTo().window(mainWindow);
	}

	/**
	 * Verifies the comments color
	 * 
	 * @param args
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws BiffException
	 */
	public void verifyCommentsColor(String values)
			throws TwfException, InterruptedException, BiffException, InvalidFormatException, IOException {
		System.out.println("---------- verifyCommentsColor----------------");
		String args[] = KWVariables.getVariables().get(values).split(":");
		List<WebElement> filter = DriverFactory.getDriver().findElements(By.xpath(args[0]));
		Thread.sleep(3000);
		for (WebElement allElement : filter) {
			String element = allElement.getText().trim();
			if (!element.equals("")) {
				int listValue = Integer.parseInt(element);
				if (listValue >= 20 && listValue <= 27) {
					if (!allElement.getAttribute(args[1]).contains(args[2]))
						Util.addExceptionToReport("Mismatch in the color", allElement.getAttribute(args[1]),
								"tablebody text colyellow");
				} else if (listValue >= 28 && listValue <= 29) {
					if (!allElement.getAttribute(args[1]).contains(args[3]))
						Util.addExceptionToReport("Mismatch in the color", allElement.getAttribute(args[1]),
								"tablebody text colorange");
				} else {
					if (!allElement.getAttribute(args[1]).contains("tablebody text colred"))
						Util.addExceptionToReport("Mismatch in the color", allElement.getAttribute(args[1]),
								"tablebody text colred");
				}
			}
		}
	}

	/**
	 * Selects multiple radio buttons
	 * 
	 * @param args
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void multipleradioBtn(String args) throws BiffException, InvalidFormatException, IOException, TwfException {
		String params = KWVariables.getVariables().get(args);
		String[] codeParam = params.split(",");
		int startValue = Integer.parseInt(codeParam[2]);
		int endValue = Integer.parseInt(codeParam[3]);
		WebDriver driver = DriverFactory.getDriver();
		for (int i = startValue; i <= endValue; i++) {
			driver.findElement(By.xpath(codeParam[0] + i + codeParam[1])).click();
		}
	}

	/**
	 * Verifies the check-box if its not not selected then it will selects
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void slecteCheckBoxOption(String values) throws BiffException, IOException, TwfException {
		System.out.println("--------------slecteCheckBoxOption ------------");
		WebElement weCheckbox = getElementByUsing(values);
		try {
			if (!weCheckbox.isSelected())
				weCheckbox.click();
		} catch (Exception e) {
			System.out.println("Checkbox is already selected");
		}
	}

	/**
	 * Verifies the review page
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyReviewPage(String values) throws Exception {
		System.out.println("---------------- code is executing -----------------");
		String[] value = KWVariables.getVariables().get(values).split("!!");
		String[] SearchParam = value[0].split("#");
		String[] codeParam = value[1].split(",");
		int startValue = Integer.parseInt(codeParam[2]);
		int endValue = Integer.parseInt(codeParam[3]);
		WebDriver driver = DriverFactory.getDriver();
		for (int i = startValue; i <= endValue; i++) {
			WebElement TableElement = driver.findElement(By.xpath(codeParam[0] + i + codeParam[1]));
			if (!TableElement.getText().trim().contains(SearchParam[i - startValue])) {
				Util.addExceptionToReport("Details mismatch", TableElement.getText().trim(),
						SearchParam[i - startValue]);
			}
		}
	}

	/**
	 * Drag and drop
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void dragAndDrop(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println(" ---------- dragAndDrop ----------");
		driver = DriverFactory.getDriver();
		String[] value = KWVariables.getVariables().get(values).split(",");
		WebElement source = getElementByUsing(value[0]);
		WebElement dest = getElementByUsing(value[0]);
		Actions act = new Actions(driver);
		act.dragAndDrop(source, dest).build().perform();
		Thread.sleep(5000);
	}

	/**
	 * Verify the Borrower Group drop-down
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyTheBorrowerGroupDD(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("------------- verifyTheBorrowerGroupDD -------------");
		String[] value = KWVariables.getVariables().get(values).split(",");
		String Storedvalue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[1]).trim();
		String valueSplit[] = Storedvalue.split(" ");
		String ExpectedValue = valueSplit[1] + ", " + valueSplit[0];
		Select borrowerGroup = new Select(getElementByUsing(value[0]));
		String selectedValueInDropDown = borrowerGroup.getFirstSelectedOption().getText().trim();
		if (!selectedValueInDropDown.equalsIgnoreCase(ExpectedValue))
			Util.addExceptionToReport("Mismatch", selectedValueInDropDown, ExpectedValue);
	}

	/**
	 * Verify pop-up With Name
	 * 
	 * @param val
	 * @throws Exception
	 */
	public void verifyPopupWithName(String val) throws Exception {
		System.out.println("*************** verifyPopupWithName **************");
		driver = DriverFactory.getDriver();
		String[] value = val.split(";");
		String ActualValue = popupHandler(value[0]);
		if (!ActualValue.contains(value[1])) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
		Thread.sleep(4000);
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("No Alert found");
		}
	}

	/**
	 * Verify Element Is Not Displayed
	 * 
	 * @param val
	 * @throws Exception
	 */
	public void verifyElementIsNotDisplayed(String val) throws Exception {
		System.out.println("************** verifyElementIsNotDisplayed ********");
		List<WebElement> Co_Borrower = DriverFactory.getDriver().findElements(By.xpath(val));
		if (Co_Borrower.size() > 0)
			Util.addExceptionToReport("Co_Borrower Information should not be displayed",
					"Co_Borrower Information is displayed", "Co_Borrower Information should not be displayed");
	}

	/**
	 * Verify the application number
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void checkForApplicationNumber(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("******************** checkForApplicationNumber *****************" + values);
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		List<WebElement> aList = driver.findElements(By.xpath(args[0]));
		int actualBranchCode = 0;
		int actualApplicationId = 0;
		for (int i = 2; i < (aList.size()); i++) {
			actualBranchCode = Integer
					.parseInt(driver.findElement(By.xpath(args[0] + "[" + i + args[1])).getText().trim());
			actualApplicationId = Integer
					.parseInt(driver.findElement(By.xpath(args[0] + "[" + i + args[2])).getText().trim());
			if (Integer.parseInt(branchCode) == actualBranchCode
					&& Integer.parseInt(applicationId) == actualApplicationId) {
				Util.addExceptionToReport("Warning info mismatch", "Application is Present",
						"It Should Not be present");
			}
		}
		getElementByUsing(args[4]).click();
		Thread.sleep(2000);
		List<WebElement> newList = driver.findElements(By.xpath(args[0]));
		if (!(newList.size() >= aList.size())) {
			Util.addExceptionToReport("Error in open and closed loans", "total loan count is less than open loans",
					"total loan count must be greater than or eqal to open loans");
		}
	}

	/**
	 * Handle payee Alert
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void handlePayeeAlert(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** handlePayeeAlert ********************");
		String args[] = values.split(";");
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(args[0]).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(1000);
		if (!getElementByUsing(args[1]).isSelected()) {
			getElementByUsing(args[1]).click();
		}
		driver.switchTo().window(winHandleBefore);
	}

	/**
	 * Select underwriter notice print options check boxs
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void selectUWActionNoticePrintOptionsCheckBox(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("***************** selectUWActionNoticePrintOptionsCheckBox *************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		if (submitToUnderwritingType != null && submitToUnderwritingType.length() > 1) {
			if (submitToUnderwritingType.equalsIgnoreCase(full_val)) {
				getElementByUsing(args[0]).click();
			} else if (submitToUnderwritingType.equalsIgnoreCase(credit_val)) {
				getElementByUsing(args[1]).click();
			}
		}
	}

	/**
	 * Verify UAS Default Information
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyUASDefaultInformation(String values) throws Exception {
		System.out.println("****************** verifyUASDefaultInformation ********************" + values);
		String args[] = KWVariables.getVariables().get(values).split(":");
		String val[] = args[0].split(",");
		String val1[] = args[1].split(",");
		for (int i = 1; i < val.length; i++) {
			String applicationDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(val[i]).trim()
					.toUpperCase();
			String underwriterDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(val1[i]).trim()
					.toUpperCase();
			if (applicationDetails.contains(".00")) {
				applicationDetails = applicationDetails.substring(0, (applicationDetails.indexOf(".")));

			}
			if (underwriterDetails.contains(".00")) {
				underwriterDetails = underwriterDetails.substring(0, (underwriterDetails.indexOf(".")));

			}
			if (applicationDetails.contains(",")) {
				applicationDetails = applicationDetails.replace(",", "");
			}
			if (underwriterDetails.contains(",")) {
				underwriterDetails = underwriterDetails.replace(",", "");
			}
			if (applicationDetails.contains("/")) {
				applicationDetails = applicationDetails.substring(0, (applicationDetails.indexOf("/")));
			}
			if (underwriterDetails.contains("/")) {
				underwriterDetails = underwriterDetails.substring(0, (underwriterDetails.indexOf("/")));
			}
			if (applicationDetails.contains("#")) {
				applicationDetails = applicationDetails.replace("#", "0");
			}
			if (underwriterDetails.contains("#")) {
				underwriterDetails = underwriterDetails.replace("#", "00");
			}
			if (applicationDetails.contains(" ")) {
				applicationDetails = applicationDetails.replace(" ", "");
			}
			if (underwriterDetails.contains(" ")) {
				underwriterDetails = underwriterDetails.replace(" ", "");
			}
			if (!underwriterDetails.contains(applicationDetails)) {
				Util.addExceptionToReport("Income Details mismatch", underwriterDetails + "", applicationDetails + "");
			}

		}
	}

	/**
	 * Select available application
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */

	public void selectAvailableApplication(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("******************** selectAvailableApplication *****************" + values);
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		String Apptype = args[5];
		String brancodeval = args[6];
		List<WebElement> aList = driver.findElements(By.xpath(args[0]));
		for (int i = 2; i < (aList.size()); i++) {
			String actualBranchCode = driver.findElement(By.xpath(args[0] + "[" + i + args[1])).getText().trim();
			String ApplicationType = driver.findElement(By.xpath(args[0] + "[" + i + args[2])).getText().trim();
			if (brancodeval.equalsIgnoreCase(actualBranchCode) && Apptype.equalsIgnoreCase(ApplicationType)) {
				driver.findElement(By.xpath(args[0] + "[" + i + args[3])).click();
				break;
			}
		}
	}

	public void checkForTheLoan(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("******************** checkForApplicationNumber *****************" + values);
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		boolean flag = false;
		List<WebElement> aList = driver.findElements(By.xpath(args[1]));
		int size = aList.size();
		if (args[0].equalsIgnoreCase("Present")) {
			int actualBranchCode = 0;
			int actualApplicationId = 0;
			for (int i = 2; i < (aList.size()); i++) {
				actualBranchCode = Integer
						.parseInt(driver.findElement(By.xpath(args[1] + "[" + i + args[2])).getText().trim());
				actualApplicationId = Integer
						.parseInt(driver.findElement(By.xpath(args[1] + "[" + i + args[3])).getText().trim());
				if (Integer.parseInt(branchCode) == actualBranchCode
						&& Integer.parseInt(applicationId) == actualApplicationId)
					flag = true;
			}
			if (flag == false)
				Util.addExceptionToReport("Warning info mismatch", "Application not Present", "It Should be present");
		} else if (args[0].equalsIgnoreCase("NotPresent")) {
			if (size > 2)
				Util.addExceptionToReport("Warning info mismatch", "Application is Present",
						"It Should Not be present");
		}
	}

	/**
	 * Verify sorting of loans in ascending and descending order
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void verifySortingAscendingDescending(String values)
			throws TwfException, InterruptedException, BiffException, InvalidFormatException, IOException {
		System.out.println("****************** verifySortingAscendingDescending **********");
		String value[] = KWVariables.getVariables().get(values).split(",");
		WebDriver driver = DriverFactory.getDriver();
		ArrayList<String> list = new ArrayList<String>();
		List<WebElement> allElement = driver.findElements(By.xpath(value[0]));
		for (int i = 2; i < allElement.size(); i++) {
			String element = driver.findElement(By.xpath(value[0] + "[" + i + value[1])).getText().trim();
			if (!element.equalsIgnoreCase(""))
				list.add(element);
		}
		List<String> tmp = new ArrayList<String>(list);
		Collections.sort(tmp);
		// List<String> reverseList = new ArrayList<String>(tmp);
		boolean sorted = tmp.equals(list);
		// boolean sorting = Ordering.natural().isOrdered(list);
		if (!sorted) {
			Util.addExceptionToReport("Records is not sorted in ascending",
					"Records is pipeline is not sorted in ascending" + list,
					"Should be sorted in ascending order" + tmp);
		}
	}

	public void verifySingleLoan(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException {
		System.out.println("******************** verifySingleLoan *****************" + values);
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		List<WebElement> aList = driver.findElements(By.xpath(args[1]));
		String actualBranchCode;
		int actualApplicationId;
		if (aList.size() == 3) {
			for (int i = 2; i < (aList.size()); i++) {
				actualBranchCode = driver.findElement(By.xpath(args[1] + "[" + i + args[2])).getText().trim();
				actualApplicationId = Integer
						.parseInt(driver.findElement(By.xpath(args[1] + "[" + i + args[3])).getText().trim());
				if (!(actualBranchCode.equals(branchCode) && actualApplicationId == Integer.parseInt(applicationId)))
					Util.addExceptionToReport("Mismatch in search", "actual ApplicationId" + applicationId,
							"Expected ApplicationId" + actualApplicationId);
			}
		} else
			Util.addExceptionToReport("Single loan should be displayed in pipeline", " actual size is" + aList.size(),
					" size expected 3");
	}

	public void verifyBranchFilter(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException {
		System.out.println("******** verifyBranchFilter *******");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		List<WebElement> aList = driver.findElements(By.xpath(args[0]));
		String actualBranchCode;
		if (aList.size() > 1) {
			for (int i = 2; i < (aList.size()); i++) {
				actualBranchCode = driver.findElement(By.xpath(args[0] + " [ " + i + args[1])).getText().trim();
				if (!actualBranchCode.equals(args[2])) {
					Util.addExceptionToReport("Mismatch in the branch code", actualBranchCode, args[2]);
				}
			}
		} else
			System.out.println("No Loans found");
	}

	/**
	 * 
	 * Assign multiple loans to underwriter
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void assignMultipleLoanToUW(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************ assignMultipleLoanToUW **********");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		ArrayList<String> list = new ArrayList<String>();
		List<WebElement> aList = driver.findElements(By.xpath(args[0]));
		ArrayList<String> UW_list = new ArrayList<String>();
		if (aList.size() > 2) {
			for (int i = 2; i <= 4; i++) {
				driver.findElement(By.xpath(args[2] + i + args[3])).click();
				String actualApplicationId = driver.findElement(By.xpath(args[0] + "[" + i + args[1])).getText().trim();
				list.add(actualApplicationId);
			}
			getElementByUsing(args[4]).click();
			Thread.sleep(5000);
			switchToParentWindow(args[8]);
			driver.switchTo().frame(args[9]);
			getElementByUsing(args[5]).click();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				Thread.sleep(2000);
			}
			Thread.sleep(8000);
			List<WebElement> UW_AppList = driver.findElements(By.xpath(args[6]));
			for (int i = 2; i < UW_AppList.size(); i++) {
				String actualApplicationId = driver.findElement(By.xpath(args[6] + "[" + i + args[7])).getText().trim();
				UW_list.add(actualApplicationId);
			}
			for (int i = 0; i < list.size(); i++) {
				if (!UW_list.contains(list.get(i)))
					Util.addExceptionToReport("Loan is not assigned to underwriter",
							"Loan is of AppiId is not assigned to underwriter" + list.get(i),
							list.get(i) + " should be present in the pipeline");
			}
		}

	}

	public void sortingDatesAscendingDescending(String values) throws BiffException, InvalidFormatException,
			IOException, ParseException, TwfException, InterruptedException {
		System.out.println("------------sortingDatesAscendingDescending-------");

		WebDriver driver = DriverFactory.getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 60);

		String value[] = KWVariables.getVariables().get(values).split(",");
		List<WebElement> dateCol = driver.findElements(By.xpath(value[0]));
		String pattern = "MM/dd/yy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		ArrayList<Date> list = new ArrayList<Date>();
		for (int i = 2; i < dateCol.size(); i++) {
			String element = driver.findElement(By.xpath(value[0] + "[" + i + value[1])).getText().trim();
			Date date = simpleDateFormat.parse(element);
			list.add(date);
		}
		try {
			WebElement nextLink = driver.findElement(By.xpath(value[6]));
			while ((driver.findElements(By.xpath(value[6])).size() > 0) && nextLink.isDisplayed()) {
				getElementByUsing(value[3]).click();
				List<WebElement> dateCols = driver.findElements(By.xpath(value[0]));
				for (int i = 2; i < dateCols.size(); i++) {
					String element = driver.findElement(By.xpath(value[0] + "[" + i + value[1])).getText().trim();
					Date date = simpleDateFormat.parse(element);
					list.add(date);
				}

			}
		} catch (StaleElementReferenceException e) {
			System.out.println("last page");
		}

		List<Date> tmp = new ArrayList<Date>(list);
		Collections.sort(tmp);
		boolean sorted = tmp.equals(list);
		if (!sorted)
			Util.addExceptionToReport("Not sorted in Ascending", list + "", tmp + "");

		getElementByUsing(value[2]).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value[4])));
		// List<WebElement> dateColDesc =
		// driver.findElements(By.xpath(value[0]));
		ArrayList<Date> listDesc = new ArrayList<Date>();
		for (int i = 2; i < dateCol.size(); i++) {
			String element = driver.findElement(By.xpath(value[0] + "[" + i + value[1])).getText().trim();
			Date date = simpleDateFormat.parse(element);
			listDesc.add(date);
		}
		try {
			WebElement nextLink = driver.findElement(By.xpath(value[6]));
			while ((driver.findElements(By.xpath(value[6])).size() > 0) && nextLink.isDisplayed()) {
				getElementByUsing(value[3]).click();
				List<WebElement> dateCols = driver.findElements(By.xpath(value[0]));
				for (int i = 2; i < dateCols.size(); i++) {
					String element = driver.findElement(By.xpath(value[0] + "[" + i + value[1])).getText().trim();
					Date date = simpleDateFormat.parse(element);
					listDesc.add(date);
				}

			}
		} catch (StaleElementReferenceException e) {
			System.out.println("last page");
		}

		List<Date> temporary = new ArrayList<Date>(listDesc);
		Collections.sort(temporary);
		Thread.sleep(1000);
		Collections.reverse(temporary);
		boolean reverse = temporary.equals(listDesc);

		if (!reverse)
			Util.addExceptionToReport("Not sorted in Descending", listDesc + "", temporary + "");
	}

	/**
	 * Verify the element displayed and not displayed status
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void isElementDisplayedAndNotDisplayed(String values) throws BiffException, IOException, TwfException {
		String value[] = values.split(";");
		boolean display = false;
		if (value[0].equalsIgnoreCase("Displayed")) {
			display = getElementByUsing(value[0]).isDisplayed();
			if (!display) {
				Util.addExceptionToReport("The element is not displayed on the webpage", "Element is not displayed",
						"Element displayed");
			}
		} else if (value[0].equalsIgnoreCase("Not Displayed")) {
			display = getElementByUsing(value[0]).isDisplayed();
			if (display) {
				Util.addExceptionToReport("The element is not displayed on the webpage", "Element is not displayed",
						"Element displayed");
			}
		}
	}

	/**
	 * Enter application id
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void enterAppID(String values) throws BiffException, IOException, TwfException, InterruptedException {
		getElementByUsing(values).clear();
		Thread.sleep(2000);
		getElementByUsing(values).sendKeys(applicationId);
	}

	/**
	 * Verify the date format conversion
	 * 
	 * @param Value
	 * @throws ParseException
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void convertDateFormat(String Value) throws ParseException, BiffException, IOException, TwfException,
			InterruptedException, InvalidFormatException {
		String args[] = KWVariables.getVariables().get(Value).split(",");
		String actualDate = getElementByUsing(args[0]).getAttribute("value").trim();
		String storedDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[1]);
		Thread.sleep(1000);
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date = formatter.parse(actualDate);
		SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy");
		String finalDate = newFormat.format(date);
		Date dateNew = formatter.parse(storedDetails);
		SimpleDateFormat newFormatNew = new SimpleDateFormat("MM/dd/yyyy");
		String currentDate = newFormatNew.format(dateNew);
		if (!finalDate.equals(currentDate)) {
			Util.addExceptionToReport("Warning info mismatch", finalDate, currentDate);
		}
	}

	/**
	 * Check the check-box if not selected
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void ifCheckboxNotSlectedClick(String values) throws BiffException, IOException, TwfException {
		System.out.println("-------------- ifCheckboxNotSlectedClick ------------");
		WebElement Checkbox = getElementByUsing(values);
		try {
			if (!Checkbox.isSelected())
				Checkbox.click();
		} catch (Exception e) {
			System.out.println("Checkbox is already selected");
		}
	}

	/**
	 * Verify UI elements names and its type
	 * 
	 * @param parms
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyUIElementType(String parms)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("********************** verifyUIElementType *******************");
		String args[] = KWVariables.getVariables().get(parms).split(":");
		String expectedUIElemntType = args[0];
		String repoIds[] = args[1].split(",");
		String actualElemntType = "";
		String tempActualType = "";
		String tempExpectedTypes = "";
		for (int i = 0; i < repoIds.length; i++) {
			switch (expectedUIElemntType) {
			case "button":
				actualElemntType = getElementByUsing(repoIds[i]).getAttribute("type").trim();
				break;
			case "checkbox":
				actualElemntType = getElementByUsing(repoIds[i]).getAttribute("type").trim();
				break;
			case "radio":
				actualElemntType = getElementByUsing(repoIds[i]).getAttribute("type").trim();
				break;
			case "select":
				actualElemntType = getElementByUsing(repoIds[i]).getTagName().trim();
				break;
			}
			if (!expectedUIElemntType.equalsIgnoreCase(actualElemntType)) {
				tempActualType = tempActualType + "," + tempActualType;
				tempExpectedTypes = tempExpectedTypes + "," + tempExpectedTypes;
			}
		}

		if (tempActualType.length() > 1) {
			Util.addExceptionToReport("UI Element types mismatch", tempActualType, tempExpectedTypes);
		}
	}

	/**
	 * Verify UI element text
	 * 
	 * @param parms
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyUIElementText(String parms)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("********************** verifyUIElementText *******************");
		String repoIds[] = KWVariables.getVariables().get(parms).split("#")[0].split(",");
		String values[] = KWVariables.getVariables().get(parms).split("#")[1].split(",");
		String expectedValues = "";
		String actualValues = "";
		for (int i = 0; i < repoIds.length; i++) {
			String tmpActual = getElementByUsing(repoIds[i]).getText().trim();
			String tmpExpected = values[i];
			if (!(tmpActual.length() > 1)) {
				tmpActual = getElementByUsing(repoIds[i]).getAttribute("value");
			}
			if (!tmpActual.equalsIgnoreCase(tmpExpected)) {
				actualValues = actualValues + "," + tmpActual;
				expectedValues = expectedValues + "," + tmpExpected;
			}
		}
		if (expectedValues.length() > 1) {
			Util.addExceptionToReport("Name mismatch : ", actualValues, expectedValues);
		}
	}

	/**
	 * Verify loan program and enter the value
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void verifyLoanPgmAndEnterValue(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		System.out.println("********************** verifyLoanPgmAndEnterValue *******************");
		String args[] = KWVariables.getVariables().get(values).split(",");
		if (getElementByUsing(args[0]).getText().equalsIgnoreCase("Refinance")) {
			getElementByUsing(args[1]).clear();
		} else if (getElementByUsing(args[0]).getText().equalsIgnoreCase("Purchase")) {
			getElementByUsing(args[1]).clear();
			getElementByUsing(args[1]).sendKeys(args[2]);
		} else {
			getElementByUsing(args[1]).clear();
			getElementByUsing(args[1]).sendKeys(args[2]);
		}
	}

	/**
	 * Verify the copied seller1 information into seller2
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void verifySellerForVA(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** verifySellerForVA ********************");
		String params = KWVariables.getVariables().get(values);
		String[] value = params.split(",");
		String SellerName = getElementByUsing(value[0]).getAttribute("value");
		String SellerAddress1 = getElementByUsing(value[1]).getAttribute("value");
		String SellerCity1 = getElementByUsing(value[2]).getAttribute("value");
		String SellerZip1 = getElementByUsing(value[3]).getAttribute("value");
		String storedName = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[4]);
		String storedAddress1 = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[5]);
		String storedCity1 = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[6]);
		String storedZip1 = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[7]);
		if (!((SellerName.equalsIgnoreCase(storedName)) || (SellerAddress1.equalsIgnoreCase(storedAddress1))
				|| (SellerCity1.equals(storedCity1)) || (SellerZip1.equalsIgnoreCase(storedZip1))))
			Util.addExceptionToReport("Mismatch in the value", "Mismatch in the value",
					"Seller value and stored value should match");
	}

	public void verifyConditions(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************* verifyConditions ************");
		String value[] = KWVariables.getVariables().get(values).split(",");
		WebDriver driver = DriverFactory.getDriver();
		String storedDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[6]).toLowerCase().trim();
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> addList = new ArrayList<String>();

		for (int i = 4; i <= 10; i++) {
			Thread.sleep(1000);
			String web = driver.findElement(By.xpath(value[0] + i + value[1])).getText().toLowerCase().trim();
			addList.add(web);
		}
		getElementByUsing(value[2]).click();
		Thread.sleep(3000);
		List<WebElement> allElement = driver.findElements(By.xpath(value[3]));
		for (WebElement ele : allElement) {
			String pending = ele.getText().toLowerCase().trim();
			list.add(pending);
		}
		for (int i = 1; i < addList.size(); i++) {
			if (!list.contains(addList.get(i)))
				Util.addExceptionToReport("Added condition is not found in pending conditions",
						"Added condition is not found in pending conditions", addList.get(i));
		}

		if (!list.contains(storedDetails))
			Util.addExceptionToReport("Added condition is not found in pending conditions",
					"Added condition is not found in pending conditions", storedDetails);
	}

	/**
	 * Verify EditPage for alert
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void verifyEditPage(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************* verifyEditPage ************");
		String value[] = values.split(",");
		WebDriver driver = DriverFactory.getDriver();
		getElementByUsing(value[1]).click();
		Thread.sleep(2000);
		try {
			Alert alert = driver.switchTo().alert();
			Thread.sleep(4000);
			if (value[0].equalsIgnoreCase("OK")) {
				alert.accept();
			}
		} catch (Exception e) {
			String title = getElementByUsing(value[2]).getText();
			if (title.contains("Application Edits")) {
				getElementByUsing(value[3]).click();
			}
		}
	}

	public void verifyAllCondition(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************* verify All Conditions ************");
		WebDriver driver = DriverFactory.getDriver();
		String[] value = KWVariables.getVariables().get(values).split("::");
		String[] SearchParam = value[0].split(":");
		String[] codeParam = value[1].split(",");
		ArrayList<String> list = new ArrayList<String>();
		int startValue;
		int endValue = driver.findElements(By.xpath(codeParam[0])).size() - 1;
		Thread.sleep(3000);
		String text = driver.findElement(By.xpath(codeParam[4])).getText();
		if (text.contains(codeParam[5])) {
			startValue = 6;
		} else {
			startValue = 5;
		}
		for (int i = startValue; i <= endValue; i++) {
			String backgroundColourCode = driver.findElement(By.xpath(codeParam[0] + "[" + i + "]"))
					.getAttribute(codeParam[2]);
			if ((backgroundColourCode == null)) {
				WebElement tableElement = driver.findElement(By.xpath(codeParam[0] + "[" + i + codeParam[1]));
				String textValue = tableElement.getText().trim();
				if (textValue.contains("*")) {
					String str = textValue.replaceAll("[\\*]", "");
					list.add(str);
				} else
					list.add(textValue);
			}
		}
		for (int i = 0; i < (SearchParam.length - 1); i++) {
			if (!list.get(i).contains(SearchParam[i])) {
				System.out.println(list.get(i));
				Util.addExceptionToReport("Details mismatch", list.get(i), SearchParam[i]);
			}
		}
	}

	/**
	 * Verify the loan processor conditions
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void conditionsCheck(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("************* click on checkbox ************");
		WebDriver driver = DriverFactory.getDriver();
		String[] codeParam = KWVariables.getVariables().get(values).split(",");
		int endValue = driver.findElements(By.xpath(codeParam[0])).size() - 1;
		Thread.sleep(3000);
		for (int i = 4; i <= endValue; i++) {
			String chkbox = driver.findElement(By.xpath(codeParam[0] + "[" + i + "]")).getAttribute(codeParam[3]);
			if (chkbox == null) {
				WebElement checkbox = driver.findElement(By.xpath(codeParam[0] + "[" + i + codeParam[1]));
				checkbox.click();
			}
		}
	}

	public void sortingTheColumn(String values) throws BiffException, InvalidFormatException, IOException,
			ParseException, TwfException, InterruptedException {
		System.out.println("------------ sortingTheColumn -------");
		System.out.println("----------------ASCENDING-----------------");
		WebDriver driver = DriverFactory.getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 60);

		String value[] = KWVariables.getVariables().get(values).split(",");
		WebElement next = getElementByUsing(value[4]);
		ArrayList<String> listAsc = new ArrayList<String>();
		List<WebElement> allElementAsc = driver.findElements(By.xpath(value[1]));
		for (int i = 2; i < (allElementAsc.size()); i++) {
			String element = driver.findElement(By.xpath(value[1] + "[" + i + value[2])).getText().trim();
			listAsc.add(element);
		}

		try {
			WebElement nextLink = driver.findElement(By.xpath(value[6]));
			int num = driver.findElements(By.xpath(value[6])).size();
			while (nextLink.isDisplayed()) {
				next.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value[3])));
				Thread.sleep(5000);
				List<WebElement> allElementsAsc = driver.findElements(By.xpath(value[1]));
				for (int i = 2; i < (allElementsAsc.size()); i++) {
					String element = driver.findElement(By.xpath(value[1] + "[" + i + value[2])).getText().trim();
					listAsc.add(element);
				}
				System.out.println("----------nextLink.isDisplayed()-------------" + nextLink.isDisplayed());
				num = driver.findElements(By.xpath(value[6])).size();
				System.out.println("----------num-------------" + num);
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("last page");
		}

		listAsc.remove("");
		Thread.sleep(1000);
		List<String> tmp = new ArrayList<String>(listAsc);
		Collections.sort(tmp);
		boolean sorted = tmp.equals(listAsc);
		if (!sorted)
			Util.addExceptionToReport("Not sorted in Ascending", listAsc + "", tmp + "");
		System.out.println("----------------DESCENDING-----------------");
		getElementByUsing(value[0]).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value[3])));
		ArrayList<String> listDesc = new ArrayList<String>();
		List<WebElement> allElementDesc = driver.findElements(By.xpath(value[1]));
		for (int i = 2; i < (allElementDesc.size()); i++) {
			String elements = driver.findElement(By.xpath(value[1] + "[" + i + value[2])).getText().trim();
			listDesc.add(elements);
		}
		boolean val = driver.findElements(By.xpath(value[6])).size() > 0;
		System.out.println("--------boolean value--------" + val);
		try {
			WebElement nextLink = driver.findElement(By.xpath(value[6]));
			while ((driver.findElements(By.xpath(value[6])).size() > 0) && nextLink.isDisplayed()) {
				driver.findElement(By.xpath(value[6])).click();
				Thread.sleep(5000);
				List<WebElement> allElementsDsc = driver.findElements(By.xpath(value[1]));
				for (int i = 2; i < (allElementsDsc.size()); i++) {
					String element = driver.findElement(By.xpath(value[1] + "[" + i + value[2])).getText().trim();
					listDesc.add(element);
				}

			}
		} catch (StaleElementReferenceException e) {
			System.out.println("last page");
		}

		listDesc.remove("");
		List<String> temporary = new ArrayList<String>(listDesc);
		Collections.sort(temporary);
		Thread.sleep(1000);
		List<String> tmpDesc = new ArrayList<String>(listAsc);
		Collections.reverse(temporary);
		boolean reverse = temporary.equals(listDesc);

		if (!reverse)
			Util.addExceptionToReport("Not sorted in Descending", listDesc + "", temporary + "");
	}

	/**
	 * verifyReviewPagerRbtn
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void verifyReviewPagerRbtn(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("------------Verify the radio btn in review page-------");
		WebDriver driver = DriverFactory.getDriver();
		// WebDriverWait wait = new WebDriverWait(driver, 60);
		String value[] = KWVariables.getVariables().get(values).split(",");
		String loanType = getElementByUsing(value[0]).getText();
		if (loanType.equals(value[2])) {
			if (driver.findElements(By.xpath(value[3])).size() > 0) {
				if (getElementByUsing(value[1]).isDisplayed())
					getElementByUsing(value[1]).click();
			}
		}
	}

	/**
	 * verifyIfCheckBoxSelectedPurchase
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyIfCheckBoxSelectedPurchase(String values) throws Exception, IOException, TwfException {
		System.out.println("************ verify If CheckBoxSelected In Review Page **********");
		WebDriver driver = DriverFactory.getDriver();
		String value[] = KWVariables.getVariables().get(values).split(",");
		String loanType = getElementByUsing(value[0]).getText();
		if (loanType.equals(value[2])) {
			if (driver.findElements(By.xpath(value[3])).size() > 0) {
				if (getElementByUsing(value[1]).isDisplayed()) {
					if (!getElementByUsing(value[1]).isSelected())
						Util.addExceptionToReport("Radio button is not selected", "Radio button is not selected",
								"Radio button should be selected");
				}
			}
		}
	}

	/**
	 * previousDate
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void previousDate(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		System.out.println("----------To Enter the Previous date-----------");
		String[] value = KWVariables.getVariables().get(values).split(":");
		DateFormat sdf = new SimpleDateFormat(value[0]); // declare as
															// DateFormat
		// Calendar today = Calendar.getInstance();
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, Integer.parseInt(value[2]));
		Date d = yesterday.getTime(); // get a Date object
		String yesterdayDate = sdf.format(d); // toString for Calendars is
												// mostly not really useful
		if (getElementByUsing(value[3]).isDisplayed()) {
			getElementByUsing(value[3]).clear();
			Thread.sleep(500);
			getElementByUsing(value[3]).sendKeys(yesterdayDate);
			Thread.sleep(1000);
		}
	}

	/**
	 * verifyJrLien
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void verifyJrLien(String values) throws TwfException, BiffException, InvalidFormatException, IOException {
		String[] value = KWVariables.getVariables().get(values).split(",");
		String storedValue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[1]).trim();
		if (storedValue.equalsIgnoreCase(applicationId))
			Util.addExceptionToReport("Application Id shouldn't be same", "Application Id is same",
					"Application Id shouldn't be same");
	}

	/**
	 * verifyRealEstateTotal
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyRealEstateTotal(String values) throws Exception {
		System.out.println("********************* verifyRealEstateTotal ***************************");
		String args[] = KWVariables.getVariables().get(values).split(",");
		float TotalValue = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[0]));
		float ValueA = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[1]));
		float ValueB = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[2]));
		float ValueC = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[3]));
		float ValueD = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[4]));
		float ExpectedValue = ValueA + ValueB + ValueC + ValueD;
		if (!(TotalValue == (ExpectedValue))) {
			Util.addExceptionToReport("Details mismatch", Float.toString(TotalValue), Float.toString(ExpectedValue));
		}
	}

	public void recurringCostAlert(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		System.out.println("---------------recurringCostAlert------------");
		String[] value = values.split(";");
		getElementByUsing(value[1]).click();
		Thread.sleep(1000);
		Alert alert = driver.switchTo().alert();
		Thread.sleep(1000);
		String alertMessage = alert.getText().trim();

		String actualValue[] = alertMessage.split("\\r?\\n");
		for (int i = 0; i < actualValue.length; i++) {
			if (!(value[2].contains(actualValue[i])))
				Util.addExceptionToReport("Mismatch in the value", actualValue[i], value[2]);
		}

		if (value[0].equalsIgnoreCase("OK")) {
			alert.accept();
		} else if (value[0].equalsIgnoreCase("CANCEL")) {
			alert.dismiss();
		}

	}

	public void selectDropdownOption(String option)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("---------------selectDropdownOption------------");

		String values[] = KWVariables.getVariables().get(option).split("::");
		WebElement we = getElementByUsing(values[0]);

		Util.selectDropdownOption(we, values[1], values[2]);
	}

	public void clickUsingJavascript(String values)
			throws TwfException, InterruptedException, BiffException, IOException {
		driver = DriverFactory.getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement link = getElementByUsing(values);
		js.executeScript("arguments[0].click();", link);
	}

	public void navigateToPageThroughHeaderTab(String values)
			throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
		String value[] = KWVariables.getVariables().get(values).split(",");
		System.out.println("--------------navigateToPageThroughHeaderTab-----------");
		System.out.println(values);
		System.out.println(value[0]);
		System.out.println(value[1]);
		driver = DriverFactory.getDriver();
		WebElement menu = getElementByUsing(value[0]);

		try {
			Actions action = new Actions(driver);
			action.moveToElement(menu).perform();
			WebElement submenu = getElementByUsing(value[1]);
			submenu.click();
		}

		catch (Exception e) {
			menu.click();
		}
	}

	public void selectDropdownOptionValue(String values) throws BiffException, IOException, TwfException {
		System.out.println("------------------selectDropdownOptionValue----------------");
		Select select = new Select(getElementByUsing(values));
		WebElement option = select.getFirstSelectedOption();
		String dropDown = option.getText();
		if (dropDown.contains("Please Select")) {
			select.selectByIndex(1);
		} else
			select.selectByIndex(2);
	}

	public void mortgageTermsAlert(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		System.out.println("---------------mortgageTermsAlert------------");
		String[] value = values.split(";");
		getElementByUsing(value[1]).click();
		Thread.sleep(1000);
		Alert alert = driver.switchTo().alert();
		Thread.sleep(1000);
		String alertMessage = alert.getText().trim();
		System.out.println("------------alertMessage-----------" + alertMessage);
		String actualValue[] = value[2].split("\\r?\\n");
		for (int i = 0; i < actualValue.length; i++)
			System.out.println("---------i------" + actualValue[i]);

		for (int i = 0; i < actualValue.length; i++) {
			if (!alertMessage.contains(actualValue[i]))
				Util.addExceptionToReport("Mismatch in the value", actualValue[i], value[2]);
		}

		if (value[0].equalsIgnoreCase("OK")) {
			alert.accept();
		} else if (value[0].equalsIgnoreCase("CANCEL")) {
			alert.dismiss();
		}

	}

	public void selectRadioBtn(String values) throws Exception {
		System.out.println("------------selectRadioBtn--------");
		String value[] = values.split(":");
		try {
			scrollToElement(value[0]);
			WebElement rbtn1 = getElementByUsing(value[0]);
			rbtn1.click();
		} catch (Exception e) {
			scrollToElement(value[1]);
			WebElement rbtn2 = getElementByUsing(value[1]);
			rbtn2.click();
		}
	}

	public void closeTheBrowser(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String originalHandle = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				driver.switchTo().window(handle);
				driver.close();
			}
		}
		driver.switchTo().window(originalHandle);
	}

	public void navigateToProcessorDateTracking(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String value[] = KWVariables.getVariables().get(values).split(",");
		Actions actions = new Actions(driver);

		try {
			WebElement target = getElementByUsing(value[0]);
			if (getElementByUsing(value[0]).isDisplayed()) {
				actions.moveToElement(target).perform();
				getElementByUsing(value[1]).click();
			}

		} catch (Exception e) {
			getElementByUsing(value[1]).click();
		}

	}

	public void selectDropDownValue(String values) throws Exception {
		String value[] = values.split(":");
		Select loanOfficer = new Select(getElementByUsing(value[0]));
		loanOfficer.selectByVisibleText(value[1]);
	}

	public void verifyTheRadioBtnIsSelected(String values) throws Exception {
		System.out.println("----------------verifyTheRadioBtnIsSelected--------------");
		String value[] = values.split(":");
		try {
			System.out.println("---------value[0]).isSelected()-------------"+getElementByUsing(value[0]).isSelected());
			if (!getElementByUsing(value[0]).isSelected())
				
				Util.addExceptionToReport("Radio button is not selected", "Radio button should be selected",
						"Radio button is not selected");
		} catch (Exception e) {
			System.out.println("---------value[0]).isSelected()-------------"+getElementByUsing(value[1]).isSelected());
			if (!getElementByUsing(value[1]).isSelected())
				Util.addExceptionToReport("Radio button is not selected", "Radio button should be selected",
						"Radio button is not selected");
		}
	}

	/*
	 *Required,Required
	 *:/html/body/form[1]/div/div[2]/table/tbody/tr[,]/td[3],2,13
	 *
	 */

	public void verifyRequiredMessageDeclaration(String values) throws Exception {
		System.out.println("****************** verifyRequiredMessageDeclaration ********************" + values);
		String[] value = KWVariables.getVariables().get(values).split(":");
		String[] SearchParam = value[0].split(",");
		String[] codeParam = value[1].split(",");
		WebDriver driver = DriverFactory.getDriver();
		int startValue = Integer.parseInt(codeParam[2]);
		int endValue = Integer.parseInt(codeParam[3]);
		for(int i=0;i<codeParam.length;i++)
			System.out.println(codeParam[i]);
		
		for (int i = startValue; i <= endValue; i++) {
			if (!(i == 4 || i == 6 || i == 10)) {
				WebElement TableElement = driver.findElement(By.xpath(codeParam[0] + i + codeParam[1]));
				if (!TableElement.getText().trim().contains(SearchParam[0])) {
					Util.addExceptionToReport("Details mismatch", TableElement.getText().trim(),
							SearchParam[i - startValue]);
				}
			}
		}
	}

	public void verifyTheSellerInformation(String values) throws Exception {
		System.out.println("****************** verifyTheSellerInformation ********************" + values);
		String[] value = KWVariables.getVariables().get(values).split(":");
		String[] webelement = value[0].split(",");
		String[] actualValue = value[1].split(",");
		String purposeOfLoan = getElementByUsing(value[3]).getText();
		WebDriver driver = DriverFactory.getDriver();
		String mainWindow = driver.getWindowHandle();
		Actions actions = new Actions(driver);
		WebElement menuOption = getElementByUsing(value[5]);
		actions.moveToElement(menuOption).perform();
		getElementByUsing(value[6]).click();
		Thread.sleep(3000);
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String windowUniqueId = i1.next();
			if (!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow)) {
				driver.switchTo().window(windowUniqueId);
				Thread.sleep(2000);
				if (purposeOfLoan.contains("Purchase")) {
					WebElement sellerName = getElementByUsing(webelement[0]);
					sellerName.clear();
					sellerName.sendKeys(actualValue[0]);

					WebElement sellerAddress = getElementByUsing(webelement[1]);
					sellerAddress.sendKeys(Keys.DELETE);
					;
					sellerAddress.sendKeys(actualValue[1]);

					WebElement sellerCity = getElementByUsing(webelement[2]);
					sellerCity.sendKeys(Keys.DELETE);
					;
					sellerCity.sendKeys(actualValue[2]);

					WebElement sellerZip = getElementByUsing(webelement[4]);
					sellerZip.sendKeys(Keys.DELETE);
					;
					sellerZip.sendKeys(actualValue[3]);

					Select select = new Select(getElementByUsing(webelement[3]));
					select.selectByVisibleText(actualValue[4]);
					getElementByUsing(value[2]).click();
					String sellerNameVal = getElementByUsing(webelement[0]).getAttribute("value");
					String sellerAddressVal = getElementByUsing(webelement[1]).getAttribute("value");
					String sellerCityVal = getElementByUsing(webelement[2]).getAttribute("value");
					String sellerZipVal = getElementByUsing(webelement[4]).getAttribute("value");
					if (!(sellerNameVal.contains(actualValue[0]) && sellerAddressVal.contains(actualValue[1])
							&& sellerCityVal.contains(actualValue[2]) && sellerZipVal.contains(actualValue[3]))) {
						Util.addExceptionToReport("Mismatch in the value",
								"Value entered and value stored must be same", "Mismatch in the value");
					}
				}
			}
		}
		driver.switchTo().window(mainWindow);
	}

	@Override
	public void checkPage() {

	}
}
